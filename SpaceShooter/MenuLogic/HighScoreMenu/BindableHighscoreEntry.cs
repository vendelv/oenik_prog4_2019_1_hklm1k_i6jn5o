﻿// <copyright file="BindableHighscoreEntry.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using GameRepository;

    /// <summary>
    /// Highscore entry class which can be displayed.
    /// </summary>
    public class BindableHighscoreEntry : HighscoreBindable
    {
        private string owner;
        private int point;
        private DateTime entryDate;
        private int killedStandard;
        private int killedBosses;
        private BindableHighscoreEntry selectedEntry;
        private int elapsedTime;
        private int difficuluty;

        /// <summary>
        /// Initializes a new instance of the <see cref="BindableHighscoreEntry"/> class.
        /// </summary>
        /// <param name="entryOwner">Player who maked this entry.</param>
        /// <param name="entryDate">Date of the entry.</param>
        /// <param name="killedStandardUnits">Count of the killed standard units.</param>
        /// <param name="killedBossUnits">Count of the killed boss units.</param>
        /// <param name="point">Number of points.</param>
        /// <param name="elapsed">Elapsed time.</param>
        /// <param name="difficulity">Difficulity of the game.</param>
        public BindableHighscoreEntry(string entryOwner, DateTime entryDate, int killedStandardUnits, int killedBossUnits, int point, int elapsed, int difficulity)
        {
            this.owner = entryOwner;
            this.entryDate = entryDate;
            this.killedBosses = killedBossUnits;
            this.killedStandard = killedStandardUnits;
            this.point = point;
            this.elapsedTime = elapsed;
            this.difficuluty = difficulity;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindableHighscoreEntry"/> class.
        /// </summary>
        /// <param name="entry">Entry which is from the repository.</param>
        public BindableHighscoreEntry(HighScoreEntry entry)
        {
            this.owner = entry.EntryOwner;
            this.entryDate = entry.EntryDate;
            this.killedBosses = entry.KilledBossUnits;
            this.killedStandard = entry.KilledStandardUnits;
            this.point = entry.Point;
            this.elapsedTime = entry.ElapsedTime;
            this.difficuluty = entry.Difficulity;
        }

        /// <summary>
        /// Gets the owner of the entry.
        /// </summary>
        public string Owner
        {
            get { return this.owner; }
        }

        /// <summary>
        /// Gets the point of the entry.
        /// </summary>
        public int Point
        {
            get { return this.point; }
        }

        /// <summary>
        /// Gets the date of the entry.
        /// </summary>
        public DateTime EntryDate
        {
            get { return this.entryDate; }
        }

        /// <summary>
        /// Gets the count of the killed standard units of the entry.
        /// </summary>
        public int KilledStandard
        {
            get { return this.killedStandard; }
        }

        /// <summary>
        /// Gets the count of the killed bosses of the entry.
        /// </summary>
        public int KilledBosses
        {
            get { return this.killedBosses; }
        }

        /// <summary>
        /// Gets or sets the count of the killed bosses of the entry.
        /// </summary>
        public BindableHighscoreEntry SelectedEntry
        {
            get { return this.selectedEntry; }
            set { this.SetField(ref this.selectedEntry, value); }
        }

        /// <summary>
        /// Gets the elapsed time of the entry in seconds.
        /// </summary>
        public int ElapsedTime
        {
            get { return this.elapsedTime; }
        }

        /// <summary>
        /// Gets the difficulity of the entry.
        /// </summary>
        public int Difficulity
        {
            get { return this.difficuluty; }
        }
    }
}
