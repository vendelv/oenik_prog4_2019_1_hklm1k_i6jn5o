﻿// <copyright file="HighscoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GameRepository;

    /// <summary>
    /// View model to display highscore entries in the menu.
    /// </summary>
    public class HighscoreViewModel : HighscoreBindable
    {
        private ObservableCollection<BindableHighscoreEntry> highscoreEntries;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreViewModel"/> class.
        /// </summary>
        public HighscoreViewModel()
        {
            this.HighscoreEntries = this.ReadEntriesFromRepository();

            this.SelectedEntry = this.HighscoreEntries.First();
            this.SelectedEntry.SelectedEntry = this.SelectedEntry;

            HighScoreWindowLogic windowLogic = new HighScoreWindowLogic();

            this.OwnerFilter = new RelayCommand((obj) =>
            {
                windowLogic.FilterByOwner(this);
            });

            this.PointsFilter = new RelayCommand((obj) =>
            {
                windowLogic.FilterByPoints(this);
            });

            this.DateFilter = new RelayCommand((obj) =>
            {
                windowLogic.FilterByDate(this);
            });
        }

        /// <summary>
        /// Gets or sets collection for high score entries.
        /// </summary>
        public ObservableCollection<BindableHighscoreEntry> HighscoreEntries
        {
            get { return this.highscoreEntries; }

            set { this.SetField(ref this.highscoreEntries, value); }
        }

        /// <summary>
        /// Gets the selected entry.
        /// </summary>
        public BindableHighscoreEntry SelectedEntry { get; private set; }

        /// <summary>
        /// Gets command for filtering the owner collumn.
        /// </summary>
        public ICommand OwnerFilter { get; private set; }

        /// <summary>
        /// Gets command for filtering the points collumn.
        /// </summary>
        public ICommand PointsFilter { get; private set; }

        /// <summary>
        /// Gets command for filterind the date collumn.
        /// </summary>
        public ICommand DateFilter { get; private set; }

        /// <summary>
        /// Reads the entries from the repository file.
        /// </summary>
        /// <returns>Returns a collection of bindable highscore entries.</returns>
        public ObservableCollection<BindableHighscoreEntry> ReadEntriesFromRepository()
        {
            HighScoreCRUDLogic crudLogic = new HighScoreCRUDLogic();
            IEnumerable<HighScoreEntry> readEntries = crudLogic.SelectAllHighScore();

            ObservableCollection<BindableHighscoreEntry> returnSet = new ObservableCollection<BindableHighscoreEntry>();

            foreach (HighScoreEntry entry in readEntries)
            {
                returnSet.Add(new BindableHighscoreEntry(entry));
            }

            // If there is no entry, add a placeholder.
            if (returnSet.Count == 0)
            {
                returnSet.Add(new BindableHighscoreEntry("Empty highscore", DateTime.Now, 0, 0, 0, 0, 0));
            }

            return returnSet;
        }
    }
}
