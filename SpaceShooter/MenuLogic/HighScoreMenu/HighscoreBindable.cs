﻿// <copyright file="HighscoreBindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Class which will be used to display highscore entries.
    /// </summary>
    public class HighscoreBindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Event handler for properties.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Invokes a property change event args event.
        /// </summary>
        /// <param name="name">Property name.</param>
        protected void OnPropertyChanged(string name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Sets the field to the new value.
        /// </summary>
        /// <typeparam name="T">Type of the field.</typeparam>
        /// <param name="field">Field.</param>
        /// <param name="newValue">New value of the field.</param>
        /// <param name="name">Name of the caller member.</param>
        protected void SetField<T>(ref T field, T newValue, [CallerMemberName] string name = null)
        {
            field = newValue;
            this.OnPropertyChanged(name);
        }
    }
}
