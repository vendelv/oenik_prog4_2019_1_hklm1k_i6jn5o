﻿// <copyright file="SecondsToStringConversationException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;

    /// <summary>
    /// Exception for seconds to string converter errors.
    /// </summary>
    public class SecondsToStringConversationException : Exception
    {
    }
}
