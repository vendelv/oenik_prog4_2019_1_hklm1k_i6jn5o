﻿// <copyright file="DateTimeToStringConversationException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;

    /// <summary>
    /// Custom exception to indicate conversation errors in DateTime To String conversation.
    /// </summary>
    public class DateTimeToStringConversationException : Exception
    {
    }
}
