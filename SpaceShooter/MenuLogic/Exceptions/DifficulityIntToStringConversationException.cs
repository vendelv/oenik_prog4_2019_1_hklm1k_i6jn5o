﻿// <copyright file="DifficulityIntToStringConversationException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;

    /// <summary>
    /// Custom exception to indicate conversation errors in Difficulity int to String conversation.
    /// </summary>
    public class DifficulityIntToStringConversationException : Exception
    {
    }
}
