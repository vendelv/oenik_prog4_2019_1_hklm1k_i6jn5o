﻿// <copyright file="DifficulityIntToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converts difficulity integer to string.
    /// </summary>
    public class DifficulityIntToStringConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                int difficulity = (int)value;

                switch (difficulity)
                {
                    case 1:
                        return "Normal";
                    case 2:
                        return "Hard";
                    case 3:
                        return "Expert";
                    default:
                        return "Unknown";
                }
            }
            else
            {
                throw new DifficulityIntToStringConversationException();
            }
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
