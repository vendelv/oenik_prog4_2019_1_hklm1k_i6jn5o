﻿// <copyright file="DateTimeToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Class which converts DateTime format to custom string.
    /// </summary>
    public class DateTimeToStringConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                DateTime dTime = (DateTime)value;

                return dTime.Year.ToString() + "/" + dTime.Month.ToString() + "/" + dTime.Day.ToString();
            }
            else
            {
                throw new DateTimeToStringConversationException();
            }
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
