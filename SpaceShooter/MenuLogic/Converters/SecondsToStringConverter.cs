﻿// <copyright file="SecondsToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converts seconds int to string in 00 format.
    /// </summary>
    public class SecondsToStringConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                int seconds = (int)value;

                int minutes = seconds / 60;
                int remaringSeconds = seconds % 60;

                return minutes.ToString("00") + ":" + remaringSeconds.ToString("00");
            }
            else
            {
                throw new SecondsToStringConversationException();
            }
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
