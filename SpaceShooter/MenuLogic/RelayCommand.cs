﻿// <copyright file="RelayCommand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Relay command, to direct commands from GUI.
    /// </summary>
    public class RelayCommand : ICommand
    {
        private Action<object> toExecute;

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="toExecute">Action to execute.</param>
        public RelayCommand(Action<object> toExecute)
        {
            this.toExecute = toExecute;
        }

        /// <summary>
        /// Event for ICommand.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Executes the action.
        /// </summary>
        /// <param name="parameter">Action to execute.</param>
        public void Execute(object parameter)
        {
            this.toExecute.Invoke(parameter);
        }

        /// <summary>
        /// Determines if the command is executable.
        /// </summary>
        /// <param name="parameter">Command.</param>
        /// <returns>Always returns true.</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
