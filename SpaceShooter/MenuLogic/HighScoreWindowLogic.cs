﻿// <copyright file="HighScoreWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MenuLogic
{
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Class for handling the background logic for the highscore window.
    /// </summary>
    public class HighScoreWindowLogic
    {
        private bool orderOwner = true;
        private bool orderPoints = true;
        private bool orderDate = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreWindowLogic"/> class.
        /// </summary>
        public HighScoreWindowLogic()
        {
        }

        /// <summary>
        /// Filter the entries by their name.
        /// </summary>
        /// <param name="model">View model.</param>
        public void FilterByOwner(HighscoreViewModel model)
        {
            if (this.orderOwner)
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderByDescending(x => x.Owner).ToList());
            }
            else
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderBy(x => x.Owner).ToList());
            }

            this.orderOwner = !this.orderOwner;
        }

        /// <summary>
        /// Filter the entries by their points.
        /// </summary>
        /// <param name="model">View model.</param>
        public void FilterByPoints(HighscoreViewModel model)
        {
            if (this.orderPoints)
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderByDescending(x => x.Point).ToList());
            }
            else
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderBy(x => x.Point).ToList());
            }

            this.orderPoints = !this.orderPoints;
        }

        /// <summary>
        /// Filter the entries by their dates.
        /// </summary>
        /// <param name="model">View model.</param>
        public void FilterByDate(HighscoreViewModel model)
        {
            if (this.orderDate)
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderByDescending(x => x.EntryDate).ToList());
            }
            else
            {
                model.HighscoreEntries = new ObservableCollection<BindableHighscoreEntry>(model.HighscoreEntries.OrderBy(x => x.EntryDate).ToList());
            }

            this.orderDate = !this.orderDate;
        }
    }
}
