﻿// <copyright file="HighScoreEntry.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System;

    /// <summary>
    /// Class which represents an entry of the high score. It contains the point which the user earned
    /// , the count of the killed units and killed bosses, the date of the entry and the name of the
    /// player who made it.
    /// </summary>
    public class HighScoreEntry
    {
        private readonly int point;

        private readonly int killedStandardUnits;

        private readonly int killedBossUnits;

        private readonly DateTime entryDate;

        private readonly string entryOwner;

        private readonly int elapsedTime;

        private readonly int difficulity;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreEntry"/> class.
        /// </summary>
        /// <param name="entryOwner">Player who maked this entry.</param>
        /// <param name="entryDate">Date of the entry.</param>
        /// <param name="killedStandardUnits">Count of the killed standard units.</param>
        /// <param name="killedBossUnits">Count of the killed boss units.</param>
        /// <param name="point">Number of points.</param>
        /// <param name="elapsed">Elapsed time of the entry.</param>
        /// <param name="difficulity">Difficulity of the entry.</param>
        public HighScoreEntry(string entryOwner, DateTime entryDate, int killedStandardUnits, int killedBossUnits, int point, int elapsed, int difficulity)
        {
            this.entryOwner = entryOwner;
            this.entryDate = entryDate;
            this.killedStandardUnits = killedStandardUnits;
            this.killedBossUnits = killedBossUnits;
            this.point = point;
            this.elapsedTime = elapsed;
            this.difficulity = difficulity;
        }

        /// <summary>
        /// Gets the point of the entry.
        /// </summary>
        public int Point
        {
            get { return this.point; }
        }

        /// <summary>
        /// Gets the count of the killed standard units for the entry.
        /// </summary>
        public int KilledStandardUnits
        {
            get { return this.killedStandardUnits; }
        }

        /// <summary>
        /// Gets the count of the killed boss units for the entry.
        /// </summary>
        public int KilledBossUnits
        {
            get { return this.killedBossUnits; }
        }

        /// <summary>
        /// Gets the date of the entry.
        /// </summary>
        public DateTime EntryDate
        {
            get { return this.entryDate; }
        }

        /// <summary>
        /// Gets the owner of the entry.
        /// </summary>
        public string EntryOwner
        {
            get { return this.entryOwner; }
        }

        /// <summary>
        /// Gets the elapsed time of the entry.
        /// </summary>
        public int ElapsedTime
        {
            get { return this.elapsedTime; }
        }

        /// <summary>
        /// Gets the dificulity of the entry.
        /// </summary>
        public int Difficulity
        {
            get { return this.difficulity; }
        }
    }
}
