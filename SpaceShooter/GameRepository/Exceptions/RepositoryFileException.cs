﻿// <copyright file="RepositoryFileException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System;

    /// <summary>
    /// Exception class which repsresetns an error connecting to IO problems.
    /// </summary>
    public class RepositoryFileException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryFileException"/> class.
        /// </summary>
        /// <param name="exceptionInfo">Text of the exception which describes the problem.</param>
        public RepositoryFileException(string exceptionInfo)
        {
            this.ExceptionInfo = exceptionInfo;
        }

        /// <summary>
        /// Gets text of the exception which describes the problem.
        /// </summary>
        public string ExceptionInfo { get; }
    }
}
