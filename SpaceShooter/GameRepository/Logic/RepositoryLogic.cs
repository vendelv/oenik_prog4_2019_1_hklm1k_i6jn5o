﻿// <copyright file="RepositoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Class which grants the mid-level logic for the highscore repository.
    /// Converts objects to strings and back.
    /// </summary>
    internal class RepositoryLogic : IRepositoryLogic
    {
        private RepositoryIOLogic ioLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryLogic"/> class.
        /// </summary>
        public RepositoryLogic()
        {
            this.ioLogic = new RepositoryIOLogic();
        }

        /// <summary>
        /// Converts the entry to string and forwards it to the IO logic.
        /// </summary>
        /// <param name="entry">Entry which will be saved.</param>
        public void SaveEntry(HighScoreEntry entry)
        {
            XElement root = new XElement("entry");
            root.Add(new XAttribute("owner", entry.EntryOwner));
            root.Add(new XAttribute("time", entry.EntryDate));
            root.Add(new XAttribute("killedStandard", entry.KilledStandardUnits));
            root.Add(new XAttribute("killedBoss", entry.KilledBossUnits));
            root.Add(new XAttribute("points", entry.Point));
            root.Add(new XAttribute("elapsed", entry.ElapsedTime));
            root.Add(new XAttribute("difficulity", entry.Difficulity));

            this.ioLogic.InsertEntryToFile(root);
        }

        /// <summary>
        /// Selects every entry from the highscore file.
        /// </summary>
        /// <returns>a...</returns>
        public IEnumerable<HighScoreEntry> SelectEntries()
        {
            return this.ioLogic.ReadXmlFile().Descendants("entry").Select(x => new HighScoreEntry(
                x.Attribute("owner").Value,
                DateTime.Parse(x.Attribute("time").Value),
                int.Parse(x.Attribute("killedStandard").Value),
                int.Parse(x.Attribute("killedBoss").Value),
                int.Parse(x.Attribute("points").Value),
                int.Parse(x.Attribute("elapsed").Value),
                int.Parse(x.Attribute("difficulity").Value)));
        }
    }
}
