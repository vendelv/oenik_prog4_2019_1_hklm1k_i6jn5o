﻿// <copyright file="RepositoryIOLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Class which creates the low-level logic for the high score repository.
    /// </summary>
    internal class RepositoryIOLogic : IRepositoryIOLogic
    {
        /// <summary>
        /// Relative location of the XML file which stores the highscores.
        /// </summary>
        internal const string XmlFileLocation = "highscore.xml";

        private static string[] emptyFileContent = { "<Highscore>", string.Empty, "</Highscore>" };

        /// <summary>
        /// Function which reads the XML file and returns the content in an XDocument format.
        /// </summary>
        /// <returns>Returns the read xml file.</returns>
        public XDocument ReadXmlFile()
        {
            // Check if the file exists, if yes, read it, if no, then create one.
            if (File.Exists(XmlFileLocation))
            {
                return XDocument.Load(XmlFileLocation);
            }
            else
            {
                XDocument highscore = CreateXmlFile();

                if (highscore != null)
                {
                    return highscore;
                }
                else
                {
                    throw new RepositoryFileException("Error during creating the highscore file!");
                }
            }
        }

        /// <summary>
        /// Insterts the string to the file.
        /// </summary>
        /// <param name="entryString">XElement of the entry which contains the attributes.</param>
        public void InsertEntryToFile(XElement entryString)
        {
            // Read the file, then get the outer node.
            XDocument highScore = this.ReadXmlFile();
            XElement node = highScore.Descendants("Highscore").FirstOrDefault();

            if (node != null)
            {
                // Add the node to the file, then save it.
                node.Add(entryString);
                highScore.Save(XmlFileLocation);
            }
        }

        /// <summary>
        /// Creates an XML file, the basic XML structure and returns it's as a XDocument content.
        /// </summary>
        /// <returns>XDocument read back from the file.</returns>
        private static XDocument CreateXmlFile()
        {
            File.WriteAllLines(XmlFileLocation, emptyFileContent);

            return XDocument.Load(XmlFileLocation);
        }
    }
}
