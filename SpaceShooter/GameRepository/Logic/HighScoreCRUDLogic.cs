﻿// <copyright file="HighScoreCRUDLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System.Collections.Generic;

    /// <summary>
    /// Class which creates the high-level logic for the high score repository.
    /// </summary>
    public class HighScoreCRUDLogic : IHighScoreCRUD
    {
        private RepositoryLogic repoLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreCRUDLogic"/> class.
        /// </summary>
        public HighScoreCRUDLogic()
        {
            this.repoLogic = new RepositoryLogic();
        }

        /// <summary>
        /// Selects every entry from the highscore file.
        /// </summary>
        /// <returns>Returns an enumeration of every highscore entry.</returns>
        public IEnumerable<HighScoreEntry> SelectAllHighScore()
        {
            return this.repoLogic.SelectEntries();
        }

        /// <summary>
        /// Passes the HighScoreEntry to the mid level logic, to save.
        /// </summary>
        /// <param name="entry">Entry which will be saved.</param>
        public void SaveEntry(HighScoreEntry entry)
        {
            this.repoLogic.SaveEntry(entry);
        }
    }
}
