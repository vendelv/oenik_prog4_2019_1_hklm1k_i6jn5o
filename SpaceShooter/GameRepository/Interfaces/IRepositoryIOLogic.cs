﻿// <copyright file="IRepositoryIOLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System.Xml.Linq;

    /// <summary>
    /// Interface which determines what logic should the lowest IO logic should implement.
    /// </summary>
    public interface IRepositoryIOLogic
    {
        /// <summary>
        /// Intserts a high score entry to the file.
        /// </summary>
        /// <param name="entryString">XElement which represents the entry in XML format.</param>
        void InsertEntryToFile(XElement entryString);

        /// <summary>
        /// Function which reads the XML file and returns the content in an XDocument format.
        /// </summary>
        /// <returns>XDocument which represents the file's content.</returns>
        XDocument ReadXmlFile();
    }
}
