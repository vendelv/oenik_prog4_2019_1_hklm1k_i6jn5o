﻿// <copyright file="IHighScoreCRUD.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface which determines which logic should the top level CRUD logic needs to implement.
    /// </summary>
    public interface IHighScoreCRUD
    {
        /// <summary>
        /// Selects every entry from the repository.
        /// </summary>
        /// <returns>Returns an enumeration of the entries.</returns>
        IEnumerable<HighScoreEntry> SelectAllHighScore();
    }
}
