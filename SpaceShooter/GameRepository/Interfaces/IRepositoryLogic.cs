﻿// <copyright file="IRepositoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepository
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface which determines which the mid level methodes the repository should impelent.
    /// </summary>
    public interface IRepositoryLogic
    {
        /// <summary>
        /// Selects every entry from the highscore file.
        /// </summary>
        /// <returns>Returns an enumeration of high score entries.</returns>
        IEnumerable<HighScoreEntry> SelectEntries();

        /// <summary>
        /// Converts the entry to string and forwars it to the IO logic.
        /// </summary>
        /// <param name="entry">Entry which will be saved.</param>
        void SaveEntry(HighScoreEntry entry);
    }
}
