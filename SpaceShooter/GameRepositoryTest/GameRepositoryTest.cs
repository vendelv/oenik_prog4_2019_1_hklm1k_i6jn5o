﻿// <copyright file="GameRepositoryTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRepositoryTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameRepository;
    using NUnit.Framework;

    /// <summary>
    /// Test class for repository.
    /// </summary>
    [TestFixture]
    public class GameRepositoryTest
    {
        /// <summary>
        /// Test that the example file is read correctly to objects.
        /// </summary>
        [Test]
        public void TestThatExampleFileReadCorrectly()
        {
            HighScoreCRUDLogic crudLogic = new HighScoreCRUDLogic();

            crudLogic.SelectAllHighScore();

            // TODO
        }
    }
}
