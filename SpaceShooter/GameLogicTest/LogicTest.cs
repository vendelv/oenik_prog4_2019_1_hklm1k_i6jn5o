﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogicTest
{
    using System;
    using System.Linq;
    using System.Threading;
    using GameLogic;
    using GameLogic.GameObjects;
    using GameLogic.GameObjects.NPCs;
    using GameLogic.Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// Tests for GameObject.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private GameModel model;
        private GameDisplay display;
        private GameLogics logic;

        private string testPlayerName = "Player";
        private int testPlayerDifficulty = 1;
        private double testWindowWidth = 1280;
        private double testWindowHeight = 720;

        /// <summary>
        /// Setup.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.model = new GameModel(this.testPlayerName, this.testPlayerDifficulty, this.testWindowWidth, this.testWindowHeight);
            this.display = new GameDisplay(this.model, this.testWindowWidth, this.testWindowHeight);
            this.logic = new GameLogics(this.model, this.display);
        }

        /// <summary>
        /// Testing that is model created properly.
        /// </summary>
        [Test]
        public void IsModelCreated()
        {
            Assert.IsNotNull(this.model);
            Assert.AreEqual(this.model.PlayerName, this.testPlayerName);
            Assert.AreEqual(this.model.Difficulty, this.testPlayerDifficulty);
            Assert.AreEqual(this.model.Player.CurrentHP, 3 / this.testPlayerDifficulty);
            Assert.AreEqual(this.model.Player.DefaultHP, 3 / this.testPlayerDifficulty);
        }

        /// <summary>
        /// Testing that is display created properly.
        /// </summary>
        [Test]
        public void IsDisplayCreated()
        {
            Assert.IsNotNull(this.display);
            Assert.AreEqual(this.display.Height, this.testWindowHeight);
            Assert.AreEqual(this.display.Width, this.testWindowWidth);
        }

        /// <summary>
        /// Testing that is logic created properly.
        /// </summary>
        [Test]
        public void IsLogicCreated()
        {
            Assert.IsNotNull(this.logic);
        }

        /// <summary>
        /// Testing that can player shoot.
        /// </summary>
        [Test]
        public void PlayerCanShoot()
        {
            Thread.Sleep(1000);
            this.model.Player.Shoot(this.model);
            this.logic.OneTick();
            Assert.AreNotEqual(this.model.OtherObjects.Where(x => x is Bullet && (x as Bullet).Owner == this.model.Player).Count(), 0);
        }

        /// <summary>
        /// Testing the player shoot delay.
        /// </summary>
        [Test]
        public void PlayerCantShootAllDay()
        {
            Thread.Sleep(1000);
            this.model.Player.Shoot(this.model);
            this.logic.OneTick();
            Thread.Sleep(50);
            this.model.Player.Shoot(this.model);
            this.logic.OneTick();
            Assert.AreEqual(this.model.OtherObjects.Where(x => x is Bullet && (x as Bullet).Owner == this.model.Player).Count(), 1);
        }

        /// <summary>
        /// Testing object collision.
        /// </summary>
        [Test]
        public void ObjectCollisionTest()
        {
            this.model.OtherObjects.Add(new Meteor(100, 100));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.Player, 0, 0));
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.OtherObjects[1]);
            this.logic.OneTick();
            Assert.AreEqual(this.model.OtherObjects.Count, 0);
        }

        /// <summary>
        /// Testing object collision.
        /// </summary>
        [Test]
        public void TestIsOutOfWindow()
        {
            this.model.OtherObjects.Add(new Meteor(9999999, 9999999));
            this.logic.OneTick();
            Assert.AreEqual(this.model.OtherObjects.Count, 0);
        }

        /// <summary>
        /// Testing all ISpawn object have their Spawn() methods.
        /// </summary>
        [Test]
        public void TestSpawnExistInISpawn()
        {
            foreach (Type spawnable in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                 .Where(mytype => mytype.GetInterfaces().Contains(typeof(ISpawn))))
            {
                Assert.IsNotNull(spawnable.GetMethod("Spawn"));
            }
        }

        /// <summary>
        /// Test if player can pick up a powerup.
        /// </summary>
        [Test]
        public void PlayerPowerupPickup()
        {
            this.model.OtherObjects.Add(new PowerupExtraLife(100, 100));
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.Player);
            this.logic.OneTick();
            Assert.AreEqual(this.model.OtherObjects.Count, 0);
        }

        /// <summary>
        /// Test if player can lose hp.
        /// </summary>
        [Test]
        public void PlayerCanGetDamage()
        {
            this.model.OtherObjects.Add(new NPCufo(0, 0, 0));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.OtherObjects[0], 0, 0));
            this.model.OtherObjects[1].XPosition = this.model.Player.XPosition - (this.model.Player.Width / 2);
            this.model.OtherObjects[1].YPosition = this.model.Player.YPosition - (this.model.Player.Height / 2);
            this.logic.OneTick();
            Assert.AreNotEqual(this.model.Player.CurrentHP, this.model.Player.DefaultHP);
        }

        /// <summary>
        /// Test if player can get points by destroy a meteor.
        /// </summary>
        [Test]
        public void PlayerGetPointFromMeteors()
        {
            this.model.OtherObjects.Add(new Meteor(100, 100));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.Player, 0, 0));
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.OtherObjects[1]);
            this.logic.OneTick();
            Assert.AreNotEqual(0, this.model.Points);
        }

        /// <summary>
        /// Test if player can get points by destroy a ufo.
        /// </summary>
        [Test]
        public void PlayerGetPointFromUfos()
        {
            this.model.OtherObjects.Add(new Meteor(100, 100));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.Player, 0, 0));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.Player, 0, 0));
            this.model.OtherObjects.Add(new Bullet(100, 100, this.model.Player, 0, 0));
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.OtherObjects[1]);
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.OtherObjects[2]);
            this.PlaceObjectToAnOther(this.model.OtherObjects[0], this.model.OtherObjects[3]);
            this.logic.OneTick();
            Assert.AreNotEqual(0, this.model.Points);
        }

        /// <summary>
        /// Test if player can move up.
        /// </summary>
        [Test]
        public void PlayerCanMoveUp()
        {
            this.model.Player.XPosition = 100;
            this.model.Player.YPosition = 100;
            this.logic.PlayerSpeed(5, true);
            this.logic.OneTick();
            Assert.AreNotEqual(100, this.model.Player.YPosition);
        }

        /// <summary>
        /// Test if player can move down.
        /// </summary>
        [Test]
        public void PlayerCanMoveDown()
        {
            this.model.Player.XPosition = 100;
            this.model.Player.YPosition = 100;
            this.logic.PlayerSpeed(-5, true);
            this.logic.OneTick();
            Assert.AreNotEqual(100, this.model.Player.YPosition);
        }

        /// <summary>
        /// Test if player can move left.
        /// </summary>
        [Test]
        public void PlayerCanMoveLeft()
        {
            this.model.Player.XPosition = 100;
            this.model.Player.YPosition = 100;
            this.logic.PlayerSpeed(-5, false);
            this.logic.OneTick();
            Assert.AreNotEqual(100, this.model.Player.XPosition);
        }

        /// <summary>
        /// Test if player can move right.
        /// </summary>
        [Test]
        public void PlayerCanMoveRight()
        {
            this.model.Player.XPosition = 100;
            this.model.Player.YPosition = 100;
            this.logic.PlayerSpeed(5, false);
            this.logic.OneTick();
            Assert.AreNotEqual(100, this.model.Player.XPosition);
        }

        private void PlaceObjectToAnOther(GameObject ob1, GameObject ob2)
        {
            ob1.XPosition = ob2.XPosition + ob2.Width;
            ob1.YPosition = ob2.YPosition + ob2.Height;
        }
    }
}
