﻿// <copyright file="LoadLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameSaveRepository
{
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Logic class to load saved games.
    /// </summary>
    public class LoadLogic
    {
        /// <summary>
        /// Loads the saved game or returns null if the file do not exists.
        /// </summary>
        /// <param name="filename">Save file location.</param>
        /// <returns>Returns an object which represents the saved game.</returns>
        public object DeserializeSaveGame(string filename)
        {
            if (File.Exists(filename))
            {
                FileStream readStream = new FileStream(filename, FileMode.Open, FileAccess.Read);

                IFormatter deserializer = new BinaryFormatter();

                return deserializer.Deserialize(readStream);
            }

            return null;
        }

        private string GetLoadLocation()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Saves\\";
        }
    }
}
