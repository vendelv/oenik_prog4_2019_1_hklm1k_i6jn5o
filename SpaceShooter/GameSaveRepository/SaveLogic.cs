﻿// <copyright file="SaveLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameSaveRepository
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Class which handles the save logic of the game.
    /// </summary>
    public class SaveLogic
    {
        /// <summary>
        /// Saves the object given in parameter.
        /// </summary>
        /// <param name="model">Object to save.</param>
        /// <returns>Return the saved file name.</returns>
        public string SerializeSaveGame(object model)
        {
            IFormatter formatter = new BinaryFormatter();

            this.CheckAndCreateDirectory();

            string name = "saveGame-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".sav";

            Stream fileStream = new FileStream(this.GetSaveLocation() + name, FileMode.Create, FileAccess.Write);

            formatter.Serialize(fileStream, model);
            fileStream.Close();

            return name;
        }

        private void CheckAndCreateDirectory()
        {
            bool exists = Directory.Exists(this.GetSaveLocation());

            if (!exists)
            {
                Directory.CreateDirectory(this.GetSaveLocation());
            }
        }

        private string GetSaveLocation()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Saves\\";
        }
    }
}
