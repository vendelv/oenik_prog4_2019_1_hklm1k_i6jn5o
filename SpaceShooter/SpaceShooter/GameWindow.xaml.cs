﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceShooter
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for Window1.xaml.
    /// </summary>
    public partial class GameWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        public GameWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="name">Player name.</param>
        /// <param name="difficulty">Difficulty.</param>
        public GameWindow(string name, int difficulty)
            : this()
        {
            this.GameControl.PlayerName = name;
            this.GameControl.Difficulty = difficulty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="fileName">Load a specific game save.</param>
        public GameWindow(string fileName)
            : this()
        {
            this.GameControl.LoadSaveFile(fileName);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Continue_Click(object sender, RoutedEventArgs e)
        {
            this.GameControl.ContinueGame();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.GameControl.SaveGame();
        }
    }
}
