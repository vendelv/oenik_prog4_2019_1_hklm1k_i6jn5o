﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceShooter
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using Microsoft.Win32;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.Difficulty.SelectedValuePath = "Key";
            this.Difficulty.DisplayMemberPath = "Value";
            this.Difficulty.Items.Add(new KeyValuePair<int, string>(1, "Normal"));
            this.Difficulty.Items.Add(new KeyValuePair<int, string>(2, "Hard"));
            this.Difficulty.Items.Add(new KeyValuePair<int, string>(3, "Expert"));
            this.Difficulty.SelectedIndex = 0;
        }

        /// <summary>
        /// New game button click handler.
        /// </summary>
        /// <param name="sender">Sender object (button?).</param>
        /// <param name="e">Event Arguments.</param>
        private void NewGameClick(object sender, RoutedEventArgs e)
        {
            string playername = this.PlayerName.Text;
            if (playername == "Player name" || playername == string.Empty)
            {
                playername = "Player";
            }

            GameWindow gw = new GameWindow(playername, int.Parse(this.Difficulty.SelectedValue.ToString()));
            gw.Closed += this.GameWindowClosed;
            gw.Show();
            this.Hide();
        }

        /// <summary>
        /// Event function, if the game window is closed.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event Arguments.</param>
        private void GameWindowClosed(object sender, EventArgs e)
        {
            this.Show();
        }

        /// <summary>
        /// Score Board button click handler.
        /// </summary>
        /// <param name="sender">Sender object (button?).</param>
        /// <param name="e">Event Arguments.</param>
        private void ScoreBoardClick(object sender, RoutedEventArgs e)
        {
            HighscoreWindow hsg = new HighscoreWindow();
            hsg.Closed += this.HighscoreWindowClosed;
            hsg.Show();
            this.Hide();
        }

        /// <summary>
        /// Event function, if the highscore window is closed.
        /// </summary>
        /// <param name="sender">Sender object..</param>
        /// <param name="e">Event Arguments..</param>
        private void HighscoreWindowClosed(object sender, EventArgs e)
        {
            this.Show();
        }

        private void PlayerName_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text == "Player name")
            {
                textBox.Text = string.Empty;
            }
        }

        private void PlayerName_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text == string.Empty)
            {
                textBox.Text = "Player name";
            }
        }

        private void LoadGame_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "save files (*.sav)|*.sav|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            if (openFileDialog.ShowDialog() == true)
            {
                GameWindow gw = new GameWindow(openFileDialog.FileName);
                gw.Closed += this.GameWindowClosed;
                gw.Show();
                this.Hide();
            }
        }
    }
}
