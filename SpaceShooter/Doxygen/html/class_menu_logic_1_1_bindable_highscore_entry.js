var class_menu_logic_1_1_bindable_highscore_entry =
[
    [ "BindableHighscoreEntry", "class_menu_logic_1_1_bindable_highscore_entry.html#ac1f284b39147671ac5e2da4774f856bc", null ],
    [ "BindableHighscoreEntry", "class_menu_logic_1_1_bindable_highscore_entry.html#aaf2b0c86e08b158312228326f0d5ca13", null ],
    [ "Difficulity", "class_menu_logic_1_1_bindable_highscore_entry.html#a18f45badb6673dfd646d5e584c2d30f1", null ],
    [ "ElapsedTime", "class_menu_logic_1_1_bindable_highscore_entry.html#acc75ab3c3f3c24ebdd628a025e5c6677", null ],
    [ "EntryDate", "class_menu_logic_1_1_bindable_highscore_entry.html#a2054572ab3f9d421f634fcb5d3a8ee52", null ],
    [ "KilledBosses", "class_menu_logic_1_1_bindable_highscore_entry.html#a0f0cab1a0b9da7c0505af15b01ce2bc2", null ],
    [ "KilledStandard", "class_menu_logic_1_1_bindable_highscore_entry.html#acf5776647ba4d5b590bd918b401c95c8", null ],
    [ "Owner", "class_menu_logic_1_1_bindable_highscore_entry.html#ae2e618205dc7fc3e2c749120b6b50790", null ],
    [ "Point", "class_menu_logic_1_1_bindable_highscore_entry.html#a75e8314f9bd62192b5ce9bebe09a8330", null ],
    [ "SelectedEntry", "class_menu_logic_1_1_bindable_highscore_entry.html#a390680bebff8d9e27fbe2fcd8b21864a", null ]
];