var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstwxy",
  1: "abdeghilmnprs",
  2: "gms",
  3: "bcdefghilmnoprst",
  4: "abcdeghklmnoprstwxy",
  5: "cp",
  6: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "events",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Events",
  6: "Pages"
};

