var searchData=
[
  ['bindablehighscoreentry',['BindableHighscoreEntry',['../class_menu_logic_1_1_bindable_highscore_entry.html',1,'MenuLogic.BindableHighscoreEntry'],['../class_menu_logic_1_1_bindable_highscore_entry.html#ac1f284b39147671ac5e2da4774f856bc',1,'MenuLogic.BindableHighscoreEntry.BindableHighscoreEntry(string entryOwner, DateTime entryDate, int killedStandardUnits, int killedBossUnits, int point, int elapsed, int difficulity)'],['../class_menu_logic_1_1_bindable_highscore_entry.html#aaf2b0c86e08b158312228326f0d5ca13',1,'MenuLogic.BindableHighscoreEntry.BindableHighscoreEntry(HighScoreEntry entry)']]],
  ['brushname',['BrushName',['../class_game_logic_1_1_game_object.html#af0c967e290b0716fdb7110368c8a4bad',1,'GameLogic::GameObject']]],
  ['builddisplay',['BuildDisplay',['../class_game_logic_1_1_game_display.html#aa8dbb7815b0e044ba932ee28e7f81988',1,'GameLogic::GameDisplay']]],
  ['bullet',['Bullet',['../class_game_logic_1_1_game_objects_1_1_bullet.html',1,'GameLogic.GameObjects.Bullet'],['../class_game_logic_1_1_game_objects_1_1_bullet.html#a711c7622ac3790ca1da0a1055a7a52a3',1,'GameLogic.GameObjects.Bullet.Bullet()']]],
  ['bulletspeed',['BulletSpeed',['../class_game_logic_1_1_game_objects_1_1_player.html#af7a964b78144de1051c33f346bfebf95',1,'GameLogic.GameObjects.Player.BulletSpeed()'],['../class_game_logic_1_1_game_objects_1_1_player_event_args_shoot.html#aafc93edc6e80b20b26e2be0fc818c38a',1,'GameLogic.GameObjects.PlayerEventArgsShoot.BulletSpeed()']]]
];
