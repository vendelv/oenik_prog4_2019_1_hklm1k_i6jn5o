var searchData=
[
  ['savedtimespan',['SavedTimeSpan',['../class_game_logic_1_1_game_model.html#a129291e1f36fa6eeb8916417c6933925',1,'GameLogic::GameModel']]],
  ['selectedentry',['SelectedEntry',['../class_menu_logic_1_1_bindable_highscore_entry.html#a390680bebff8d9e27fbe2fcd8b21864a',1,'MenuLogic.BindableHighscoreEntry.SelectedEntry()'],['../class_menu_logic_1_1_highscore_view_model.html#a0794ca996b1cd8f131437249c42b2e3d',1,'MenuLogic.HighscoreViewModel.SelectedEntry()']]],
  ['shootdelay',['ShootDelay',['../class_game_logic_1_1_game_objects_1_1_player.html#a289b7c2a0e9c2d8e14389c8fb1a360be',1,'GameLogic::GameObjects::Player']]],
  ['shoots',['Shoots',['../class_game_logic_1_1_game_model.html#a50ff997df99afb2dc06d9d0de1d19a20',1,'GameLogic::GameModel']]],
  ['speed',['Speed',['../class_game_logic_1_1_game_objects_1_1_bullet.html#a22446297ef8932e81bc89017ddc22695',1,'GameLogic.GameObjects.Bullet.Speed()'],['../class_game_logic_1_1_game_objects_1_1_meteor.html#a2ffa7597c4a75b6e1fd233edc39360e6',1,'GameLogic.GameObjects.Meteor.Speed()'],['../class_game_logic_1_1_game_objects_1_1_powerup_base.html#ad1f82c5daeb994d6082fedf221f3408e',1,'GameLogic.GameObjects.PowerupBase.Speed()']]]
];
