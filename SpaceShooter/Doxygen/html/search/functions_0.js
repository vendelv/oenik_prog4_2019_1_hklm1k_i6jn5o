var searchData=
[
  ['bindablehighscoreentry',['BindableHighscoreEntry',['../class_menu_logic_1_1_bindable_highscore_entry.html#ac1f284b39147671ac5e2da4774f856bc',1,'MenuLogic.BindableHighscoreEntry.BindableHighscoreEntry(string entryOwner, DateTime entryDate, int killedStandardUnits, int killedBossUnits, int point, int elapsed, int difficulity)'],['../class_menu_logic_1_1_bindable_highscore_entry.html#aaf2b0c86e08b158312228326f0d5ca13',1,'MenuLogic.BindableHighscoreEntry.BindableHighscoreEntry(HighScoreEntry entry)']]],
  ['builddisplay',['BuildDisplay',['../class_game_logic_1_1_game_display.html#aa8dbb7815b0e044ba932ee28e7f81988',1,'GameLogic::GameDisplay']]],
  ['bullet',['Bullet',['../class_game_logic_1_1_game_objects_1_1_bullet.html#a711c7622ac3790ca1da0a1055a7a52a3',1,'GameLogic::GameObjects::Bullet']]]
];
