var searchData=
[
  ['testisoutofwindow',['TestIsOutOfWindow',['../class_game_logic_test_1_1_logic_test.html#a18071bfcb3166f78352bc18b910fa4da',1,'GameLogicTest::LogicTest']]],
  ['testspawnexistinispawn',['TestSpawnExistInISpawn',['../class_game_logic_test_1_1_logic_test.html#a0f715a42b0ec25873dc55354093ebd3b',1,'GameLogicTest::LogicTest']]],
  ['testthatexamplefilereadcorrectly',['TestThatExampleFileReadCorrectly',['../class_game_repository_test_1_1_game_repository_test.html#a54b96bd6b287927509a87f94dc35d7ed',1,'GameRepositoryTest::GameRepositoryTest']]],
  ['ticking',['Ticking',['../class_game_logic_1_1_game_objects_1_1_bullet.html#a579788acb26e0b2e3fe6c5dafeadc018',1,'GameLogic.GameObjects.Bullet.Ticking()'],['../class_game_logic_1_1_game_objects_1_1_meteor.html#af23b5dca5bb205c36452053aecb0ae9d',1,'GameLogic.GameObjects.Meteor.Ticking()'],['../class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html#acaf4e617cb84ea7dd4c762bc09f416f6',1,'GameLogic.GameObjects.NPCBoss.Ticking()'],['../class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html#a25f7fe12bc4a5d1a84097b84538d2737',1,'GameLogic.GameObjects.NPCs.NPCufo.Ticking()'],['../class_game_logic_1_1_game_objects_1_1_powerup_base.html#aa8cdfdb711078a95f996cbf488c0c27a',1,'GameLogic.GameObjects.PowerupBase.Ticking()'],['../interface_game_logic_1_1_interfaces_1_1_i_ticking.html#a7e9faeec7ed51aca96d416a4a47c17fa',1,'GameLogic.Interfaces.ITicking.Ticking()']]],
  ['timeelapsed',['TimeElapsed',['../class_game_logic_1_1_game_model.html#a41fa5131a84128f59bf58a92f7b160ec',1,'GameLogic::GameModel']]]
];
