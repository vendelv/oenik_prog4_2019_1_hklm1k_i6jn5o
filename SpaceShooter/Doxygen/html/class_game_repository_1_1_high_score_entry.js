var class_game_repository_1_1_high_score_entry =
[
    [ "HighScoreEntry", "class_game_repository_1_1_high_score_entry.html#a10f3afb5f17e25849f279a7e580b54de", null ],
    [ "Difficulity", "class_game_repository_1_1_high_score_entry.html#ad5810d39bf80cb0d27e8561f7e67427d", null ],
    [ "ElapsedTime", "class_game_repository_1_1_high_score_entry.html#af9e234ae6712b4562b3ca1e235099b46", null ],
    [ "EntryDate", "class_game_repository_1_1_high_score_entry.html#a32724642dfd39aba26b32ef91c07b9e2", null ],
    [ "EntryOwner", "class_game_repository_1_1_high_score_entry.html#aa96f66f7a9121bcdd8b1d8efbbdd58b7", null ],
    [ "KilledBossUnits", "class_game_repository_1_1_high_score_entry.html#a9de6115064616d340507d2e03b6029f2", null ],
    [ "KilledStandardUnits", "class_game_repository_1_1_high_score_entry.html#a098565ff3019701be733995fea82fc82", null ],
    [ "Point", "class_game_repository_1_1_high_score_entry.html#ac212a6bb91ee561dc452551d21896da0", null ]
];