var namespaces_dup =
[
    [ "GameLogic", "namespace_game_logic.html", "namespace_game_logic" ],
    [ "GameLogicTest", "namespace_game_logic_test.html", null ],
    [ "GameRepository", "namespace_game_repository.html", null ],
    [ "GameRepositoryTest", "namespace_game_repository_test.html", null ],
    [ "GameSaveRepository", "namespace_game_save_repository.html", null ],
    [ "MenuLogic", "namespace_menu_logic.html", null ],
    [ "SpaceShooter", "namespace_space_shooter.html", "namespace_space_shooter" ]
];