var namespace_game_logic_1_1_interfaces =
[
    [ "ICanDie", "interface_game_logic_1_1_interfaces_1_1_i_can_die.html", "interface_game_logic_1_1_interfaces_1_1_i_can_die" ],
    [ "ISolid", "interface_game_logic_1_1_interfaces_1_1_i_solid.html", "interface_game_logic_1_1_interfaces_1_1_i_solid" ],
    [ "ISpawn", "interface_game_logic_1_1_interfaces_1_1_i_spawn.html", null ],
    [ "ITicking", "interface_game_logic_1_1_interfaces_1_1_i_ticking.html", "interface_game_logic_1_1_interfaces_1_1_i_ticking" ]
];