var namespace_menu_logic =
[
    [ "BindableHighscoreEntry", "class_menu_logic_1_1_bindable_highscore_entry.html", "class_menu_logic_1_1_bindable_highscore_entry" ],
    [ "DateTimeToStringConversationException", "class_menu_logic_1_1_date_time_to_string_conversation_exception.html", null ],
    [ "DateTimeToStringConverter", "class_menu_logic_1_1_date_time_to_string_converter.html", "class_menu_logic_1_1_date_time_to_string_converter" ],
    [ "DifficulityIntToStringConversationException", "class_menu_logic_1_1_difficulity_int_to_string_conversation_exception.html", null ],
    [ "DifficulityIntToStringConverter", "class_menu_logic_1_1_difficulity_int_to_string_converter.html", "class_menu_logic_1_1_difficulity_int_to_string_converter" ],
    [ "HighscoreBindable", "class_menu_logic_1_1_highscore_bindable.html", "class_menu_logic_1_1_highscore_bindable" ],
    [ "HighscoreViewModel", "class_menu_logic_1_1_highscore_view_model.html", "class_menu_logic_1_1_highscore_view_model" ],
    [ "HighScoreWindowLogic", "class_menu_logic_1_1_high_score_window_logic.html", "class_menu_logic_1_1_high_score_window_logic" ],
    [ "RelayCommand", "class_menu_logic_1_1_relay_command.html", "class_menu_logic_1_1_relay_command" ],
    [ "SecondsToStringConversationException", "class_menu_logic_1_1_seconds_to_string_conversation_exception.html", null ],
    [ "SecondsToStringConverter", "class_menu_logic_1_1_seconds_to_string_converter.html", "class_menu_logic_1_1_seconds_to_string_converter" ]
];