var class_game_logic_1_1_game_objects_1_1_powerup_base =
[
    [ "PowerupBase", "class_game_logic_1_1_game_objects_1_1_powerup_base.html#a6fdcd534a046b27c6611e26f767e4e8e", null ],
    [ "Collision", "class_game_logic_1_1_game_objects_1_1_powerup_base.html#a6fe5466c055ea2c48479ad97943866e2", null ],
    [ "Effect", "class_game_logic_1_1_game_objects_1_1_powerup_base.html#ae210e9fb6fbf938ff995bcde88a46126", null ],
    [ "Ticking", "class_game_logic_1_1_game_objects_1_1_powerup_base.html#aa8cdfdb711078a95f996cbf488c0c27a", null ],
    [ "Speed", "class_game_logic_1_1_game_objects_1_1_powerup_base.html#ad1f82c5daeb994d6082fedf221f3408e", null ]
];