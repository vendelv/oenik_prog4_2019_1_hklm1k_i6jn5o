var hierarchy =
[
    [ "Application", null, [
      [ "GameLogic.App", "class_game_logic_1_1_app.html", null ],
      [ "GameLogic.App", "class_game_logic_1_1_app.html", null ],
      [ "GameLogic.App", "class_game_logic_1_1_app.html", null ],
      [ "SpaceShooter.App", "class_space_shooter_1_1_app.html", null ],
      [ "SpaceShooter.App", "class_space_shooter_1_1_app.html", null ],
      [ "SpaceShooter.App", "class_space_shooter_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "GameLogic.Properties.Settings", "class_game_logic_1_1_properties_1_1_settings.html", null ],
      [ "SpaceShooter.Properties.Settings", "class_space_shooter_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "EventArgs", null, [
      [ "GameLogic.GameObjects.PlayerEventArgsCollision", "class_game_logic_1_1_game_objects_1_1_player_event_args_collision.html", null ],
      [ "GameLogic.GameObjects.PlayerEventArgsShoot", "class_game_logic_1_1_game_objects_1_1_player_event_args_shoot.html", null ]
    ] ],
    [ "Exception", null, [
      [ "GameRepository.RepositoryFileException", "class_game_repository_1_1_repository_file_exception.html", null ],
      [ "MenuLogic.DateTimeToStringConversationException", "class_menu_logic_1_1_date_time_to_string_conversation_exception.html", null ],
      [ "MenuLogic.DifficulityIntToStringConversationException", "class_menu_logic_1_1_difficulity_int_to_string_conversation_exception.html", null ],
      [ "MenuLogic.SecondsToStringConversationException", "class_menu_logic_1_1_seconds_to_string_conversation_exception.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "GameLogic.GameControl", "class_game_logic_1_1_game_control.html", null ]
    ] ],
    [ "GameLogic.GameDisplay", "class_game_logic_1_1_game_display.html", null ],
    [ "GameLogic.GameLogics", "class_game_logic_1_1_game_logics.html", null ],
    [ "GameLogic.GameModel", "class_game_logic_1_1_game_model.html", null ],
    [ "GameLogic.GameObject", "class_game_logic_1_1_game_object.html", [
      [ "GameLogic.GameObjects.Bullet", "class_game_logic_1_1_game_objects_1_1_bullet.html", null ],
      [ "GameLogic.GameObjects.Meteor", "class_game_logic_1_1_game_objects_1_1_meteor.html", null ],
      [ "GameLogic.GameObjects.Misc.Explosion", "class_game_logic_1_1_game_objects_1_1_misc_1_1_explosion.html", null ],
      [ "GameLogic.GameObjects.NPCBoss", "class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html", null ],
      [ "GameLogic.GameObjects.NPCs.NPCufo", "class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html", null ],
      [ "GameLogic.GameObjects.Player", "class_game_logic_1_1_game_objects_1_1_player.html", null ],
      [ "GameLogic.GameObjects.PowerupBase", "class_game_logic_1_1_game_objects_1_1_powerup_base.html", [
        [ "GameLogic.GameObjects.PowerupBomb", "class_game_logic_1_1_game_objects_1_1_powerup_bomb.html", null ],
        [ "GameLogic.GameObjects.PowerupExtraLife", "class_game_logic_1_1_game_objects_1_1_powerup_extra_life.html", null ],
        [ "GameLogic.GameObjects.PowerupExtraShoot", "class_game_logic_1_1_game_objects_1_1_powerup_extra_shoot.html", null ],
        [ "GameLogic.GameObjects.PowerupExtraSpeed", "class_game_logic_1_1_game_objects_1_1_powerup_extra_speed.html", null ],
        [ "GameLogic.GameObjects.PowerupSpeedyShoot", "class_game_logic_1_1_game_objects_1_1_powerup_speedy_shoot.html", null ],
        [ "GameLogic.GameObjects.PowerupThreeShoot", "class_game_logic_1_1_game_objects_1_1_powerup_three_shoot.html", null ]
      ] ]
    ] ],
    [ "GameRepositoryTest.GameRepositoryTest", "class_game_repository_test_1_1_game_repository_test.html", null ],
    [ "GameRepository.HighScoreEntry", "class_game_repository_1_1_high_score_entry.html", null ],
    [ "MenuLogic.HighScoreWindowLogic", "class_menu_logic_1_1_high_score_window_logic.html", null ],
    [ "GameLogic.Interfaces.ICanDie", "interface_game_logic_1_1_interfaces_1_1_i_can_die.html", null ],
    [ "GameLogic.ICanShoot", "interface_game_logic_1_1_i_can_shoot.html", [
      [ "GameLogic.GameObjects.NPCBoss", "class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html", null ],
      [ "GameLogic.GameObjects.NPCs.NPCufo", "class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html", null ],
      [ "GameLogic.GameObjects.Player", "class_game_logic_1_1_game_objects_1_1_player.html", null ]
    ] ],
    [ "ICommand", null, [
      [ "MenuLogic.RelayCommand", "class_menu_logic_1_1_relay_command.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "SpaceShooter.GameWindow", "class_space_shooter_1_1_game_window.html", null ],
      [ "SpaceShooter.GameWindow", "class_space_shooter_1_1_game_window.html", null ],
      [ "SpaceShooter.HighscoreWindow", "class_space_shooter_1_1_highscore_window.html", null ],
      [ "SpaceShooter.HighscoreWindow", "class_space_shooter_1_1_highscore_window.html", null ],
      [ "SpaceShooter.MainWindow", "class_space_shooter_1_1_main_window.html", null ],
      [ "SpaceShooter.MainWindow", "class_space_shooter_1_1_main_window.html", null ]
    ] ],
    [ "GameRepository.IHighScoreCRUD", "interface_game_repository_1_1_i_high_score_c_r_u_d.html", [
      [ "GameRepository.HighScoreCRUDLogic", "class_game_repository_1_1_high_score_c_r_u_d_logic.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "MenuLogic.HighscoreBindable", "class_menu_logic_1_1_highscore_bindable.html", [
        [ "MenuLogic.BindableHighscoreEntry", "class_menu_logic_1_1_bindable_highscore_entry.html", null ],
        [ "MenuLogic.HighscoreViewModel", "class_menu_logic_1_1_highscore_view_model.html", null ]
      ] ]
    ] ],
    [ "GameLogic.INpcControlled", "interface_game_logic_1_1_i_npc_controlled.html", null ],
    [ "GameRepository.IRepositoryIOLogic", "interface_game_repository_1_1_i_repository_i_o_logic.html", [
      [ "GameRepository.RepositoryIOLogic", "class_game_repository_1_1_repository_i_o_logic.html", null ]
    ] ],
    [ "GameRepository.IRepositoryLogic", "interface_game_repository_1_1_i_repository_logic.html", [
      [ "GameRepository.RepositoryLogic", "class_game_repository_1_1_repository_logic.html", null ]
    ] ],
    [ "GameLogic.Interfaces.ISolid", "interface_game_logic_1_1_interfaces_1_1_i_solid.html", [
      [ "GameLogic.GameObjects.Bullet", "class_game_logic_1_1_game_objects_1_1_bullet.html", null ],
      [ "GameLogic.GameObjects.Meteor", "class_game_logic_1_1_game_objects_1_1_meteor.html", null ],
      [ "GameLogic.GameObjects.NPCBoss", "class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html", null ],
      [ "GameLogic.GameObjects.NPCs.NPCufo", "class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html", null ],
      [ "GameLogic.GameObjects.Player", "class_game_logic_1_1_game_objects_1_1_player.html", null ],
      [ "GameLogic.GameObjects.PowerupBase", "class_game_logic_1_1_game_objects_1_1_powerup_base.html", null ]
    ] ],
    [ "GameLogic.Interfaces.ISpawn", "interface_game_logic_1_1_interfaces_1_1_i_spawn.html", [
      [ "GameLogic.GameObjects.Meteor", "class_game_logic_1_1_game_objects_1_1_meteor.html", null ],
      [ "GameLogic.GameObjects.NPCBoss", "class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html", null ],
      [ "GameLogic.GameObjects.NPCs.NPCufo", "class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html", null ],
      [ "GameLogic.GameObjects.PowerupBase", "class_game_logic_1_1_game_objects_1_1_powerup_base.html", null ]
    ] ],
    [ "GameLogic.Interfaces.ITicking", "interface_game_logic_1_1_interfaces_1_1_i_ticking.html", [
      [ "GameLogic.GameObjects.Bullet", "class_game_logic_1_1_game_objects_1_1_bullet.html", null ],
      [ "GameLogic.GameObjects.Meteor", "class_game_logic_1_1_game_objects_1_1_meteor.html", null ],
      [ "GameLogic.GameObjects.NPCBoss", "class_game_logic_1_1_game_objects_1_1_n_p_c_boss.html", null ],
      [ "GameLogic.GameObjects.NPCs.NPCufo", "class_game_logic_1_1_game_objects_1_1_n_p_cs_1_1_n_p_cufo.html", null ],
      [ "GameLogic.GameObjects.PowerupBase", "class_game_logic_1_1_game_objects_1_1_powerup_base.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "MenuLogic.DateTimeToStringConverter", "class_menu_logic_1_1_date_time_to_string_converter.html", null ],
      [ "MenuLogic.DifficulityIntToStringConverter", "class_menu_logic_1_1_difficulity_int_to_string_converter.html", null ],
      [ "MenuLogic.SecondsToStringConverter", "class_menu_logic_1_1_seconds_to_string_converter.html", null ]
    ] ],
    [ "GameSaveRepository.LoadLogic", "class_game_save_repository_1_1_load_logic.html", null ],
    [ "GameLogicTest.LogicTest", "class_game_logic_test_1_1_logic_test.html", null ],
    [ "SpaceShooter.Properties.Resources", "class_space_shooter_1_1_properties_1_1_resources.html", null ],
    [ "GameLogic.Properties.Resources", "class_game_logic_1_1_properties_1_1_resources.html", null ],
    [ "GameSaveRepository.SaveLogic", "class_game_save_repository_1_1_save_logic.html", null ],
    [ "Window", null, [
      [ "SpaceShooter.GameWindow", "class_space_shooter_1_1_game_window.html", null ],
      [ "SpaceShooter.GameWindow", "class_space_shooter_1_1_game_window.html", null ],
      [ "SpaceShooter.GameWindow", "class_space_shooter_1_1_game_window.html", null ],
      [ "SpaceShooter.HighscoreWindow", "class_space_shooter_1_1_highscore_window.html", null ],
      [ "SpaceShooter.HighscoreWindow", "class_space_shooter_1_1_highscore_window.html", null ],
      [ "SpaceShooter.HighscoreWindow", "class_space_shooter_1_1_highscore_window.html", null ],
      [ "SpaceShooter.MainWindow", "class_space_shooter_1_1_main_window.html", null ],
      [ "SpaceShooter.MainWindow", "class_space_shooter_1_1_main_window.html", null ],
      [ "SpaceShooter.MainWindow", "class_space_shooter_1_1_main_window.html", null ]
    ] ]
];