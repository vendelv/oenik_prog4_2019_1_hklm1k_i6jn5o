var class_game_logic_test_1_1_logic_test =
[
    [ "IsDisplayCreated", "class_game_logic_test_1_1_logic_test.html#aca3955b96009a4dd8c148a6d09a76040", null ],
    [ "IsLogicCreated", "class_game_logic_test_1_1_logic_test.html#a671de2279c8c57976d6cc1bb888152fb", null ],
    [ "IsModelCreated", "class_game_logic_test_1_1_logic_test.html#aa9f3f511605a8bfb7b908f190be2ddca", null ],
    [ "ObjectCollisionTest", "class_game_logic_test_1_1_logic_test.html#a2ecbe8b4120c129e6aa90ad20ac9f4cd", null ],
    [ "PlayerCanGetDamage", "class_game_logic_test_1_1_logic_test.html#a3febe69382cec644add663bb32626ee1", null ],
    [ "PlayerCanMoveDown", "class_game_logic_test_1_1_logic_test.html#a97560277104b834a3fcdb9c0d07c1210", null ],
    [ "PlayerCanMoveLeft", "class_game_logic_test_1_1_logic_test.html#a5935695f6266294f3cff9871802398ea", null ],
    [ "PlayerCanMoveRight", "class_game_logic_test_1_1_logic_test.html#ab7ae50a74549c48a7bead042ba4d3fdd", null ],
    [ "PlayerCanMoveUp", "class_game_logic_test_1_1_logic_test.html#a627e785fd71c6c52a69ab2abaa89c4a2", null ],
    [ "PlayerCanShoot", "class_game_logic_test_1_1_logic_test.html#a3dd8652fa6698d4686bf364cea5cdd46", null ],
    [ "PlayerCantShootAllDay", "class_game_logic_test_1_1_logic_test.html#a87dbe57f1d3ba567b83e0be180763035", null ],
    [ "PlayerGetPointFromMeteors", "class_game_logic_test_1_1_logic_test.html#aee1fc4ce088c2e30f4a28e6048702d43", null ],
    [ "PlayerGetPointFromUfos", "class_game_logic_test_1_1_logic_test.html#a81aedbd082b346bd74968dd7b49263af", null ],
    [ "PlayerPowerupPickup", "class_game_logic_test_1_1_logic_test.html#af6e2fe0c52ad6865bf6bb8e40c6b63b8", null ],
    [ "Setup", "class_game_logic_test_1_1_logic_test.html#af4fc788c9c64832f9ccb0f5dbe3b1b45", null ],
    [ "TestIsOutOfWindow", "class_game_logic_test_1_1_logic_test.html#a18071bfcb3166f78352bc18b910fa4da", null ],
    [ "TestSpawnExistInISpawn", "class_game_logic_test_1_1_logic_test.html#a0f715a42b0ec25873dc55354093ebd3b", null ]
];