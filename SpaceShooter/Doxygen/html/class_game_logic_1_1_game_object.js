var class_game_logic_1_1_game_object =
[
    [ "GameObject", "class_game_logic_1_1_game_object.html#ad43c39627b6d57ba24ca9e9b5351a9a3", null ],
    [ "GetBrush", "class_game_logic_1_1_game_object.html#af5ed62611756b0b54545924d12722691", null ],
    [ "IsCollision", "class_game_logic_1_1_game_object.html#a4cc0be4611c0a9d76efeffcd715bb2dd", null ],
    [ "SetObjectArea", "class_game_logic_1_1_game_object.html#a40220600d7bbb01d0f829234250c11a3", null ],
    [ "Area", "class_game_logic_1_1_game_object.html#a098e5b21c9aaf68b0f93ff3c7c9fc912", null ],
    [ "BrushName", "class_game_logic_1_1_game_object.html#af0c967e290b0716fdb7110368c8a4bad", null ],
    [ "CurrentHP", "class_game_logic_1_1_game_object.html#ad5abf6170282008e9415d2d52ca05317", null ],
    [ "DefaultHP", "class_game_logic_1_1_game_object.html#af821147c1d5c17335971aaa0d4357644", null ],
    [ "Degree", "class_game_logic_1_1_game_object.html#a1146dcb707f661b422eabff6cfded299", null ],
    [ "Height", "class_game_logic_1_1_game_object.html#a1025a6cf4fe8548ffb5a57df0b56ec86", null ],
    [ "NotRemoveIfOutOfScreen", "class_game_logic_1_1_game_object.html#aeb8bf309bec47c81a2cc98b7003c962c", null ],
    [ "Rad", "class_game_logic_1_1_game_object.html#aeba4dab9e6a07428deac91a2e65b792d", null ],
    [ "RealArea", "class_game_logic_1_1_game_object.html#a6e618313b1ddfd02ea9c0c3631e2729a", null ],
    [ "Width", "class_game_logic_1_1_game_object.html#aa19e043f85dd55a88338fdcb72fc08f5", null ],
    [ "XPosition", "class_game_logic_1_1_game_object.html#a39d0e12c9e36f9d11269bdcd1ab20218", null ],
    [ "YPosition", "class_game_logic_1_1_game_object.html#a0233d88ebbd5cf11b06f041d46338a85", null ]
];