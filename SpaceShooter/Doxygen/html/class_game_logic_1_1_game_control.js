var class_game_logic_1_1_game_control =
[
    [ "GameControl", "class_game_logic_1_1_game_control.html#aa1f9146ac5d9b5840415246ab5357200", null ],
    [ "ContinueGame", "class_game_logic_1_1_game_control.html#a0c23ac0500832323862ec2d717ef8623", null ],
    [ "LoadSaveFile", "class_game_logic_1_1_game_control.html#a019a504d5c85989ea61f97ee80b2e0aa", null ],
    [ "OnRender", "class_game_logic_1_1_game_control.html#a8607c1e01e8928cab2dc0ea974f34f20", null ],
    [ "PauseGame", "class_game_logic_1_1_game_control.html#a14c37427a7b28375928dfd3a1b5c757c", null ],
    [ "SaveGame", "class_game_logic_1_1_game_control.html#a9b06ed8c27c31a9ebc564c722b3cba73", null ],
    [ "Difficulty", "class_game_logic_1_1_game_control.html#aea9ecafc03d3b41b88e9d6c75f683e00", null ],
    [ "PlayerName", "class_game_logic_1_1_game_control.html#a190338a494d4303f0c597ff606ad3603", null ]
];