var annotated_dup =
[
    [ "GameLogic", "namespace_game_logic.html", "namespace_game_logic" ],
    [ "GameLogicTest", "namespace_game_logic_test.html", "namespace_game_logic_test" ],
    [ "GameRepository", "namespace_game_repository.html", "namespace_game_repository" ],
    [ "GameRepositoryTest", "namespace_game_repository_test.html", "namespace_game_repository_test" ],
    [ "GameSaveRepository", "namespace_game_save_repository.html", "namespace_game_save_repository" ],
    [ "MenuLogic", "namespace_menu_logic.html", "namespace_menu_logic" ],
    [ "SpaceShooter", "namespace_space_shooter.html", "namespace_space_shooter" ]
];