﻿// <copyright file="NPCBoss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Windows;
    using GameLogic.GameObjects.NPCs;
    using GameLogic.Interfaces;

    /// <summary>
    /// Class which represents a boss object.
    /// </summary>
    [Serializable]
    public class NPCBoss : NPC, ICanShoot, ISpawn
    {
        private static Random rand = new Random();
        private static double lastKilled;
        private double lastShoot;

        /// <summary>
        /// Initializes a new instance of the <see cref="NPCBoss"/> class.
        /// </summary>
        /// <param name="width">Boss's width.</param>
        /// <param name="height">Boss's height.</param>
        /// <param name="xPosition">X start position.</param>
        /// <param name="yPosition">Y start position.</param>
        /// <param name="hp">Boss's hp.</param>
        public NPCBoss(double width, double height, double xPosition, double yPosition, int hp)
            : base(width, height, xPosition, yPosition, hp)
        {
            this.BrushName = "boss_01.png";
            this.SetObjectArea();
        }

        /// <summary>
        /// Spawn logic for NPCBoss.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public static void Spawn(GameModel model, GameDisplay display)
        {
            if (model.TimeElapsed.TotalMilliseconds < lastKilled)
            {
                lastKilled = 0.0;
            }

            if (!model.OtherObjects.Exists(x => x is NPCBoss) && model.TimeElapsed.TotalMilliseconds - lastKilled > 60000 - rand.Next(0, 10001))
            {
                model.OtherObjects.Add(new NPCBoss(175, 250, (double)rand.Next(0, (int)display.Width), 0, 20 * model.Difficulty));
            }
        }

        /// <summary>
        /// Ticking logic for NPCBoss.
        /// </summary>
        /// <param name="model">Boss's width.</param>
        /// <param name="display">Boss's height.</param>
        public override void Ticking(GameModel model, GameDisplay display)
        {
            base.Ticking(model, display);

            if (rand.Next(0, this.InAheadPosition() ? 10 : 30) == 0)
            {
                this.Shoot(model);
            }
        }

        /// <summary>
        /// Collision logic for the boss.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void Collision(GameModel model)
        {
            base.Collision(model);

            if (this.CurrentHP <= 0)
            {
                model.KilledBoss++;
                model.Points += 50;
                lastKilled = model.TimeElapsed.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Shoots to the player.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Shoot(GameModel model)
        {
            if (model.TimeElapsed.TotalMilliseconds - this.lastShoot >= 500)
            {
                int rnd = rand.Next(0, 101);

                if (rnd < 10)
                {
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 25));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, -25));
                }
                else if (rnd < 20)
                {
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 30));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 20));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 10));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, -10));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, -20));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, -30));
                }
                else if (rnd < 40)
                {
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) - 30, this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) + 30, this.YPosition - (this.Height / 4), this, 10, 0));
                }
                else if (rnd < 60)
                {
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) - 30, this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) - 15, this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) + 15, this.YPosition - (this.Height / 4), this, 10, 0));
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4) + 30, this.YPosition - (this.Height / 4), this, 10, 0));
                }
                else
                {
                    model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10));
                }

                this.lastShoot = model.TimeElapsed.TotalMilliseconds;
            }
        }
    }
}
