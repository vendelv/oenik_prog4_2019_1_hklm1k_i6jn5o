﻿// <copyright file="NPCufo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects.NPCs
{
    using System;
    using System.Windows;
    using GameLogic.GameObjects.Misc;
    using GameLogic.Interfaces;

    /// <summary>
    /// NPCenemy.
    /// </summary>
    [Serializable]
    public class NPCufo : NPC, ICanShoot, ISpawn
    {
        private const int SIZE = 100;
        private static Random rand = new Random();
        private static double lastSpawn;
        private double lastShoot;

        /// <summary>
        /// Initializes a new instance of the <see cref="NPCufo"/> class.
        /// </summary>
        /// <param name="width">Ufo's width.</param>
        /// <param name="height">Ufo's height.</param>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        /// <param name="hp">Object hp.</param>
        public NPCufo(double width, double height, double xPosition, double yPosition, int hp)
            : base(width, height, xPosition, yPosition - (height / 2), hp)
        {
            this.BrushName = "enemy.png";
            this.SetObjectArea();
        }

        /// <summary>
        /// Spawn logic for the ufo.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public static void Spawn(GameModel model, GameDisplay display)
        {
            if (model.TimeElapsed.TotalMilliseconds < lastSpawn)
            {
                lastSpawn = 0.0;
            }

            if (model.TimeElapsed.TotalMilliseconds - lastSpawn > rand.Next(5000, 20000))
            {
                model.OtherObjects.Add(new NPCufo(SIZE, SIZE, (double)rand.Next(0, (int)display.Width), 0, 1 * model.Difficulty));
                lastSpawn = model.TimeElapsed.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Collision logic for the enemy.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void Collision(GameModel model)
        {
            base.Collision(model);

            if (this.CurrentHP <= 0)
            {
                model.KilledUnits++;
                model.Points += 5;
            }
        }

        /// <summary>
        /// Moveing logic for the enemy.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public override void Ticking(GameModel model, GameDisplay display)
        {
            base.Ticking(model, display);

            if (rand.Next(0, this.InAheadPosition() ? 20 : 100) == 0)
            {
                this.Shoot(model);
            }
        }

        /// <summary>
        /// Shoot logic for the object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Shoot(GameModel model)
        {
            if (model.TimeElapsed.TotalMilliseconds - this.lastShoot >= 500)
            {
                model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - (this.Height / 4), this, 10));
                this.lastShoot = model.TimeElapsed.TotalMilliseconds;
            }
        }
    }
}
