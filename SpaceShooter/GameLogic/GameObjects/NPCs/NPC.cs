﻿// <copyright file="NPC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects.NPCs
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic.GameObjects.Misc;
    using GameLogic.Interfaces;

    /// <summary>
    /// NPCenemy.
    /// </summary>
    [Serializable]
    public class NPC : GameObject, ISolid, ITicking
    {
        private static Random rand = new Random();
        private Point ahead = default(Point);
        private int speed;

        /// <summary>
        /// Initializes a new instance of the <see cref="NPC"/> class.
        /// </summary>
        /// <param name="width">Width.</param>
        /// <param name="height">Height.</param>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        /// <param name="hp">Object hp.</param>
        public NPC(double width, double height, double xPosition, double yPosition, int hp)
            : base(width, height, xPosition, yPosition - (height / 2), hp)
        {
            this.SetObjectArea();
        }

        /// <summary>
        /// Collision logic for the enemy.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public virtual void Collision(GameModel model)
        {
            foreach (GameObject item in model.OtherObjects.FindAll(x => x is Bullet))
            {
                if ((item as Bullet).Owner is Player && this.IsCollision(item))
                {
                    this.CurrentHP--;
                    if (this.CurrentHP <= 0)
                    {
                        model.OtherObjects.Remove(this);
                        new Explosion(model, this);
                    }

                    model.OtherObjects.Remove(item);
                }
            }
        }

        /// <summary>
        /// Moveing logic for the enemy.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public virtual void Ticking(GameModel model, GameDisplay display)
        {
            if (this.InAheadPosition() && rand.Next(0, 10) == 0)
            {
                this.ahead = new Point((double)rand.Next((int)this.Width, (int)(display.Width - (this.Width / 2))), rand.Next((int)this.Height, (int)(display.Height - (this.Height / 2))));
                this.speed = (int)(rand.Next(4, 7) * ((model.TimeElapsed.Minutes / 60000 / 4) + 1));
            }
            else if (!this.InAheadPosition())
            {
                var vector = new Point(this.ahead.X - this.XPosition, this.ahead.Y - this.YPosition);
                var length = Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
                var unitVector = new Point(vector.X / length, vector.Y / length);
                this.XPosition = this.XPosition + (unitVector.X * this.speed);
                this.YPosition = this.YPosition + (unitVector.Y * this.speed);
            }
        }

        /// <summary>
        /// Get's if the NPC is in the decided position.
        /// </summary>
        /// <returns>Is in that position.</returns>
        protected bool InAheadPosition()
        {
            if (this.ahead.X == 0 || this.ahead.Y == 0)
            {
                return true;
            }

            return this.ahead.X < this.XPosition + 10 && this.ahead.X > this.XPosition - 10 && this.ahead.Y < this.YPosition + 10 && this.ahead.Y > this.YPosition - 10;
        }
    }
}