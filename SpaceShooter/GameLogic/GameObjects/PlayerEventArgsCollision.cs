﻿// <copyright file="PlayerEventArgsCollision.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;

    /// <summary>
    /// PlayerShoot event args.
    /// </summary>
    public class PlayerEventArgsCollision : EventArgs
    {
        /// <summary>
        /// Gets or sets the other GameObject which collided.
        /// </summary>
        public GameObject OtherObject { get; set; }
    }
}
