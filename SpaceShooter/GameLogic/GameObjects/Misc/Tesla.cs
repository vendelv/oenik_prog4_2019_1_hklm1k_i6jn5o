﻿// <copyright file="Tesla.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using GameLogic.Interfaces;

    /// <summary>
    /// Meteor gameobject.
    /// </summary>
    [Serializable]
    public class Tesla : GameObject, ITicking
    {
        private static Random rnd = new Random();
        private int rnddegree = 1;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tesla"/> class.
        /// </summary>
        /// <param name="xPosition">X start position.</param>
        /// <param name="yPosition">Y start position.</param>
        public Tesla(double xPosition, double yPosition)
            : base(100, 70, xPosition, yPosition, 0)
        {
            this.BrushName = "tesla.png";
            if (rnd.Next(0, 2) == 0)
            {
                this.rnddegree = -1;
            }

            this.SetObjectArea();
        }

        /// <summary>
        /// Moveing object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public void Ticking(GameModel model, GameDisplay display)
        {
            this.XPosition -= 5;
            this.YPosition -= 2;
            this.Degree += this.rnddegree * 0.75;
        }
    }
}
