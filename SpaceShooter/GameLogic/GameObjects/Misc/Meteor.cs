﻿// <copyright file="Meteor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic.Interfaces;

    /// <summary>
    /// Meteor gameobject.
    /// </summary>
    [Serializable]
    public class Meteor : GameObject, ISolid, ITicking, ISpawn
    {
        private const int METEORMINSPEED = 5;
        private const int METEORMAXSPEED = 10;
        private const double METEORSIZE = 100;
        private const int MAXMETEOR = 10;
        private static Random rand = new Random();
        private static double lastMeteor;

        /// <summary>
        /// Initializes a new instance of the <see cref="Meteor"/> class.
        /// </summary>
        /// <param name="xPosition">X start position.</param>
        /// <param name="yPosition">Y start position.</param>
        public Meteor(double xPosition, double yPosition)
            : base(METEORSIZE, METEORSIZE, xPosition, yPosition, 0)
        {
            this.Speed = rand.Next(METEORMINSPEED, METEORMAXSPEED + 1);
            this.BrushName = "asteroid.png";
            this.SetObjectArea();
        }

        /// <summary>
        /// Gets the meteor speed.
        /// </summary>
        public double Speed { get; private set; }

        /// <summary>
        /// Spawn logic for Meteors.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public static void Spawn(GameModel model, GameDisplay display)
        {
            if (model.TimeElapsed.TotalMilliseconds < lastMeteor)
            {
                lastMeteor = 0.0;
            }

            if (model.TimeElapsed.TotalMilliseconds - lastMeteor > rand.Next(1000, 5000) && model.OtherObjects.FindAll(x => x is Meteor).Count < MAXMETEOR)
            {
                model.OtherObjects.Add(new Meteor(rand.Next(100, int.Parse(display.Width.ToString()) - 100), 0));
                lastMeteor = model.TimeElapsed.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Collision logic for Meteors.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Collision(GameModel model)
        {
            foreach (GameObject item in model.OtherObjects.FindAll(x => x is Bullet))
            {
                if (this.IsCollision(item))
                {
                    if ((item as Bullet).Owner == model.Player)
                    {
                        model.Points += 1;
                    }

                    model.OtherObjects.Remove(this);
                    model.OtherObjects.Remove(item);
                }
            }
        }

        /// <summary>
        /// Moveing object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public void Ticking(GameModel model, GameDisplay display)
        {
            this.YPosition += this.Speed * ((model.TimeElapsed.Minutes / 60000 / 3) + 1);
        }
    }
}
