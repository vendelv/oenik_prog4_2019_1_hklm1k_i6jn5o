﻿// <copyright file="Explosion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects.Misc
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Explosion gameobject.
    /// </summary>
    [Serializable]
    public class Explosion : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Explosion"/> class.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="obj">Object which will be replaced with this Explosion.</param>
        public Explosion(GameModel model, GameObject obj)
            : this(model, obj.Width, obj.Height, obj.XPosition, obj.YPosition)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Explosion"/> class.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="width">Original object width.</param>
        /// <param name="height">Original object height.</param>
        /// <param name="xPosition">Original object X position.</param>
        /// <param name="yPosition">Original object Y position.</param>
        public Explosion(GameModel model, double width, double height, double xPosition, double yPosition)
            : base(width, height, xPosition, yPosition, 0)
        {
            this.BrushName = "explosion.png";
            this.SetObjectArea();
            model.OtherObjects.Add(this);
            Task.Delay(1000).ContinueWith(x =>
            {
                this.RemoveExplosion(model);
            });
        }

        /// <summary>
        /// Remove explosion from GameModel.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void RemoveExplosion(GameModel model)
        {
            lock (model.LockOtherObjects)
            {
                model.OtherObjects.Remove(this);
            }
        }
    }
}
