﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic.Interfaces;

    /// <summary>
    /// Bullet object.
    /// </summary>
    [Serializable]
    public class Bullet : GameObject, ISolid, ITicking
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="xPosition">X start position.</param>
        /// <param name="yPosition">Y start position.</param>
        /// <param name="owner">Bullet owner.</param>
        /// <param name="speed">Bullet speed.</param>
        /// <param name="angel">Bullet angel.</param>
        /// <param name="width">Bullet width.</param>
        /// <param name="height">Bullet height.</param>
        public Bullet(double xPosition, double yPosition, GameObject owner, double speed = 10, double angel = 0, double width = 5, double height = 20)
            : base(width, height, xPosition, yPosition, 0)
        {
            this.Speed = speed;
            this.Owner = owner;
            this.Degree = angel;

            this.SetObjectArea();
        }

        /// <summary>
        /// Gets or sets bullet's speed in pixel per tick.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// Gets bullet's owner.
        /// </summary>
        public GameObject Owner { get; private set; }

        /// <summary>
        /// Collision logic for Bullets (it dose nothing!).
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Collision(GameModel model)
        {
            "Do nothing".ToString();
        }

        /// <summary>
        /// Get the bullet brush.
        /// </summary>
        /// <returns>Brush.</returns>
        public override Brush GetBrush()
        {
            if (this.BrushName == null)
            {
                return this.Owner is Player ? Brushes.Green : Brushes.Red;
            }

            base.GetBrush();
            return null;
        }

        /// <summary>
        /// Sets the area of the object.
        /// </summary>
        public override void SetObjectArea()
        {
            this.Area = new RectangleGeometry(new Rect(-this.Width / 2, this.Height / 2, this.Width, this.Height));
        }

        /// <summary>
        /// Moveing object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public void Ticking(GameModel model, GameDisplay display)
        {
            this.YPosition += this.Speed;
            this.XPosition += this.Speed * -Math.Sin(this.Degree / 180);
        }
    }
}
