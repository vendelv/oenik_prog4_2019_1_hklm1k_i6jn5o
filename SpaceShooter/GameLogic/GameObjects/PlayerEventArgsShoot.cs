﻿// <copyright file="PlayerEventArgsShoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;

    /// <summary>
    /// PlayerShoot event args.
    /// </summary>
    public class PlayerEventArgsShoot : EventArgs
    {
        /// <summary>
        /// Gets or sets BulletSpeed event args.
        /// </summary>
        public int BulletSpeed { get; set; }
    }
}
