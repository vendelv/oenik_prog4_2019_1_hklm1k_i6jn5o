﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic.Interfaces;

    /// <summary>
    /// Player object.
    /// </summary>
    [Serializable]
    public class Player : GameObject, ICanShoot, ISolid
    {
        private const int TIMEBETWEENSHOOTS = 400;
        private const int BULLETSPEED = 10;
        private const int MAXSPEED = 5;
        private double lastShoot;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="width">Player width.</param>
        /// <param name="height">Player height.</param>
        /// <param name="xPosition">X start position.</param>
        /// <param name="yPosition">Y start position.</param>
        /// <param name="hp">Player default lifes.</param>
        public Player(double width, double height, double xPosition, double yPosition, int hp)
            : base(width, height, xPosition, yPosition, hp)
        {
            this.MaxSpeed = MAXSPEED;
            this.ShootDelay = TIMEBETWEENSHOOTS;
            this.BulletSpeed = BULLETSPEED;
            this.BrushName = "player.png";
            this.SetObjectArea();
        }

        /// <summary>
        /// Gets the default value of delay between shoots.
        /// </summary>
        public static int DefaultShootDelay
        {
            get { return TIMEBETWEENSHOOTS; }
        }

        /// <summary>
        /// Gets the default value of delay between shoots.
        /// </summary>
        public static int DefaultBulletSpeed
        {
            get { return BULLETSPEED; }
        }

        /// <summary>
        /// Gets the default player speed.
        /// </summary>
        public static int DefaultMaxSpeed
        {
            get { return MAXSPEED; }
        }

        /// <summary>
        /// Gets or sets player X speed.
        /// </summary>
        public double XSpeed { get; set; }

        /// <summary>
        /// Gets or sets player Y speed.
        /// </summary>
        public double YSpeed { get; set; }

        /// <summary>
        /// Gets or sets the player max speed.
        /// </summary>
        public int MaxSpeed { get; set; }

        /// <summary>
        /// Gets or sets the delay between shoots.
        /// </summary>
        public int ShootDelay { get; set; }

        /// <summary>
        /// Gets or sets player's bullet speed.
        /// </summary>
        public int BulletSpeed { get; set; }

#pragma warning disable SA1201 // Elements should appear in the correct order
        /// <summary>
        /// This event calls when the player is shooting.
        /// </summary>
        public event EventHandler<PlayerEventArgsShoot> PlayerShoot;

        /// <summary>
        /// This event calls when the player is shooting.
        /// </summary>
        public event EventHandler<PlayerEventArgsCollision> PlayerCollision;
#pragma warning restore SA1201 // Elements should appear in the correct order

        /// <summary>
        /// Collision logic for the player.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Collision(GameModel model)
        {
            foreach (GameObject item in model.OtherObjects.FindAll(x => x is Bullet || x is Meteor))
            {
                if ((!(item is Bullet) || (item as Bullet).Owner != this) && this.IsCollision(item))
                {
                    if (this.PlayerCollision != null)
                    {
                        PlayerEventArgsCollision arg = new PlayerEventArgsCollision();
                        arg.OtherObject = item;
                        this.PlayerCollision.Invoke(this, arg);
                    }

                    model.Player.CurrentHP--;
                    model.OtherObjects.Remove(item);
                }
            }
        }

        /// <summary>
        /// Shoot logic for the object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Shoot(GameModel model)
        {
            if (model.TimeElapsed.TotalMilliseconds - this.lastShoot >= this.ShootDelay || model.TimeElapsed.TotalMilliseconds < this.lastShoot)
            {
                if (this.PlayerShoot != null)
                {
                    PlayerEventArgsShoot arg = new PlayerEventArgsShoot();
                    arg.BulletSpeed = -this.BulletSpeed;
                    this.PlayerShoot.Invoke(this, arg);
                }

                model.Shoots++;
                model.OtherObjects.Add(new Bullet(this.XPosition - (this.Width / 4), this.YPosition - this.Height + 20, this, -this.BulletSpeed));
                this.lastShoot = model.TimeElapsed.TotalMilliseconds;
            }
        }
    }
}