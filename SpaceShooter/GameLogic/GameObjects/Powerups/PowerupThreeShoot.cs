﻿// <copyright file="PowerupThreeShoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Increase the bullet amount for the player, uppon pick up.
    /// </summary>
    [Serializable]
    public class PowerupThreeShoot : PowerupBase
    {
        private GameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupThreeShoot"/> class.
        /// </summary>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        public PowerupThreeShoot(double xPosition, double yPosition)
            : base(xPosition, yPosition)
        {
            this.BrushName = "powerup_threeshoot.png";
            this.Duration = TimeSpan.FromSeconds(10);
        }

        /// <summary>
        /// Effect logic for <see cref="PowerupThreeShoot"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectStart(GameModel model)
        {
            this.model = model;
            model.Player.PlayerShoot += this.Player_PlayerShoot;
        }

        /// <summary>
        /// Effect end logic for <see cref="PowerupThreeShoot"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectEnd(GameModel model)
        {
            model.Player.PlayerShoot -= this.Player_PlayerShoot;
        }

        private void Player_PlayerShoot(object sender, PlayerEventArgsShoot e)
        {
            if (sender != null && sender is Player)
            {
                Player player = sender as Player;
                this.model.OtherObjects.Add(new Bullet(player.XPosition, player.YPosition - player.Height + 20, player, -player.BulletSpeed));
                this.model.OtherObjects.Add(new Bullet(player.XPosition - (player.Width / 2), player.YPosition - player.Height + 20, player, -player.BulletSpeed));
            }
        }
    }
}
