﻿// <copyright file="PowerupBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic.Interfaces;

    /// <summary>
    /// Base class for powerups.
    /// </summary>
    [Serializable]
    public class PowerupBase : GameObject, ITicking, ISolid, ISpawn
    {
        private const double SPEED = 5;
        private const double SIZE = 50;
        private static Random rand = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupBase"/> class.
        /// </summary>
        /// <param name="xPosition">X position.</param>
        /// <param name="yPosition">Y position.</param>
        public PowerupBase(double xPosition, double yPosition)
            : base(SIZE, SIZE, xPosition, yPosition, 0)
        {
            this.Speed = SPEED;
            this.SetObjectArea();
        }

        /// <summary>
        /// Gets or sets the PowerUp duration.
        /// </summary>
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// Gets the PowerUp speed.
        /// </summary>
        public double Speed { get; private set; }

        /// <summary>
        /// Spawn object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public static void Spawn(GameModel model, GameDisplay display)
        {
            if (rand.Next(0, 1000) == 0)
            {
                switch (rand.Next(0, 6))
                {
                    case 0:
                        if (model.Player.CurrentHP < 5)
                        {
                            model.OtherObjects.Add(new PowerupExtraLife(rand.Next(100, (int)display.Width - 100), 0));
                        }

                        break;
                    case 1:
                        model.OtherObjects.Add(new PowerupExtraShoot(rand.Next(100, (int)display.Width - 100), 0));
                        break;
                    case 2:
                        model.OtherObjects.Add(new PowerupExtraSpeed(rand.Next(100, (int)display.Width - 100), 0));
                        break;
                    case 3:
                        model.OtherObjects.Add(new PowerupSpeedyShoot(rand.Next(100, (int)display.Width - 100), 0));
                        break;
                    case 4:
                        model.OtherObjects.Add(new PowerupBomb(rand.Next(100, (int)display.Width - 100), 0));
                        break;
                    case 5:
                        model.OtherObjects.Add(new PowerupThreeShoot(rand.Next(100, (int)display.Width - 100), 0));
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// MUST BE OVERRIDE!!! Effect start logic for PowerUps.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public virtual void EffectStart(GameModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// MUST BE OVERRIDE!!! Effect end logic for PowerUps.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public virtual void EffectEnd(GameModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Collision logic for PowerUps.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public void Collision(GameModel model)
        {
            if (this.IsCollision(model.Player))
            {
                this.EffectStart(model);
                if (this.Duration != TimeSpan.Zero)
                {
                    model.PlayerPowerups.Add(this, model.TimeElapsed);
                }

                model.OtherObjects.Remove(this);
            }
        }

        /// <summary>
        /// Moveing object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public void Ticking(GameModel model, GameDisplay display)
        {
            this.YPosition += this.Speed;
        }
    }
}
