﻿// <copyright file="PowerupExtraLife.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;

    /// <summary>
    /// Gives the player +1 life, uppon pick up.
    /// </summary>
    [Serializable]
    public class PowerupExtraLife : PowerupBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupExtraLife"/> class.
        /// </summary>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        public PowerupExtraLife(double xPosition, double yPosition)
            : base(xPosition, yPosition)
        {
            this.BrushName = "heart.png";
        }

        /// <summary>
        /// Effect start logic for <see cref="PowerupExtraLife"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectStart(GameModel model)
        {
            model.Player.CurrentHP++;
        }
    }
}
