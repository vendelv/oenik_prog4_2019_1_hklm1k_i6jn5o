﻿// <copyright file="PowerupExtraShoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Increase the bullet amount for the player, uppon pick up.
    /// </summary>
    [Serializable]
    public class PowerupExtraShoot : PowerupBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupExtraShoot"/> class.
        /// </summary>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        public PowerupExtraShoot(double xPosition, double yPosition)
            : base(xPosition, yPosition)
        {
            this.BrushName = "powerup_bullet.png";
            this.Duration = TimeSpan.FromSeconds(10);
        }

        /// <summary>
        /// Effect start logic for <see cref="PowerupExtraShoot"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectStart(GameModel model)
        {
            model.Player.ShootDelay = model.Player.ShootDelay / 2;
        }

        /// <summary>
        /// Effect end logic for <see cref="PowerupExtraShoot"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectEnd(GameModel model)
        {
            model.Player.ShootDelay = model.Player.ShootDelay * 2;
        }
    }
}
