﻿// <copyright file="PowerupBomb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.GameObjects
{
    using System;

    /// <summary>
    /// Gives the player +1 life, uppon pick up.
    /// </summary>
    [Serializable]
    public class PowerupBomb : PowerupBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupBomb"/> class.
        /// </summary>
        /// <param name="xPosition">X Position.</param>
        /// <param name="yPosition">Y Position.</param>
        public PowerupBomb(double xPosition, double yPosition)
            : base(xPosition, yPosition)
        {
            this.BrushName = "powerup_bomb.png";
        }

        /// <summary>
        /// Effect start logic for <see cref="PowerupExtraLife"/>.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public override void EffectStart(GameModel model)
        {
            model.OtherObjects.RemoveAll(x => !(x is PowerupBase) && !(x is NPCBoss));
        }
    }
}
