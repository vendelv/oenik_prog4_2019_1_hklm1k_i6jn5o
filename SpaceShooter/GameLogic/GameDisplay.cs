﻿// <copyright file="GameDisplay.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Draw everything to the canvas.
    /// </summary>
    public class GameDisplay
    {
        private GameModel model;
        private Rect bgrect;

        // Assets
        private Pen red = new Pen(Brushes.Red, 1);
        private Pen green = new Pen(Brushes.Green, 1);
        private Pen gray = new Pen(Brushes.Gray, 0.5);
        private Pen transparent = new Pen(Brushes.Transparent, 1);
        private Typeface arial = new Typeface("Arial");

        /// <summary>
        /// Initializes a new instance of the <see cref="GameDisplay"/> class.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="w">Window Width.</param>
        /// <param name="h">Window Height.</param>
        public GameDisplay(GameModel model, double w, double h)
        {
            this.model = model;
            this.Width = w;
            this.Height = h;
            this.bgrect = new Rect(0, 0, this.Width, this.Height);
        }

        /// <summary>
        /// Gets the window width.
        /// </summary>
        public double Width { get; private set; }

        /// <summary>
        /// Gets the window height.
        /// </summary>
        public double Height { get; private set; }

        /// <summary>
        /// Main drawing method.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        /// <param name="lastFrameTime">Last frametime from GameControl.</param>
        public void BuildDisplay(DrawingContext ctx, long lastFrameTime)
        {
            this.DrawPlayer(ctx);
            this.DrawOtherObject(ctx);

            this.DrawLifes(ctx);
            this.DrawPowerups(ctx);
            this.DrawPoints(ctx);
            this.DrawKilledBoss(ctx);
            this.DrawKilledUnit(ctx);
            this.DrawTime(ctx);

            this.DrawObjectCount(ctx);
            this.DrawFPS(ctx, lastFrameTime);
            this.DrawInfo(ctx);
        }

        /// <summary>
        /// Draw Lifes.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawLifes(DrawingContext ctx)
        {
            BitmapImage img = new BitmapImage(new Uri("Resources/heart.png", UriKind.Relative));
            for (int i = 0; i < this.model.Player.CurrentHP; i++)
            {
                ctx.DrawImage(img, new Rect(10 + (30 * i), 10, 30, 30));
            }
        }

        /// <summary>
        /// Draw PowerUps.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawPowerups(DrawingContext ctx)
        {
            int i = 0;
            foreach (var powerup in this.model.PlayerPowerups)
            {
                BitmapImage img = new BitmapImage(new Uri("Resources/" + powerup.Key.BrushName, UriKind.Relative));
                ctx.DrawImage(img, new Rect(10 + (30 * i), 40, 30, 30));
                i++;
            }
        }

        /// <summary>
        /// Draw Player points.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawPoints(DrawingContext ctx)
        {
            FormattedText text = new FormattedText(
                "Point: " + this.model.Points.ToString(),
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                30,
                Brushes.White);
            Point textLocation = new Point(this.Width - 10 - text.WidthIncludingTrailingWhitespace, 10);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw Killed Boss count.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawKilledBoss(DrawingContext ctx)
        {
            FormattedText text = new FormattedText(
                "Boss: " + this.model.KilledBoss.ToString(),
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                20,
                Brushes.White);
            Point textLocation = new Point(this.Width - 10 - text.WidthIncludingTrailingWhitespace, 45);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw Killed Units count.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawKilledUnit(DrawingContext ctx)
        {
            FormattedText text = new FormattedText(
                "Unit: " + this.model.KilledUnits.ToString(),
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                20,
                Brushes.White);
            Point textLocation = new Point(this.Width - 10 - text.WidthIncludingTrailingWhitespace, 65);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw elapsed time.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawTime(DrawingContext ctx)
        {
            string answer = string.Format(
                "{0:D2}:{1:D2}",
                this.model.TimeElapsed.Minutes,
                this.model.TimeElapsed.Seconds);
            FormattedText text = new FormattedText(
                answer,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                20,
                Brushes.White);
            Point textLocation = new Point(this.Width - 10 - text.WidthIncludingTrailingWhitespace, 90);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw FPS.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        /// <param name="lastFrameTime">Last frametime from GameControl.</param>
        private void DrawFPS(DrawingContext ctx, long lastFrameTime)
        {
            FormattedText text = new FormattedText(
                "FrameTime: " + (lastFrameTime == 0 ? "<" : string.Empty) + lastFrameTime + "ms",
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                10,
                Brushes.Gray);

            Point textLocation = new Point(5, this.Height - 35);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw Object Count (debug).
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawObjectCount(DrawingContext ctx)
        {
            FormattedText text = new FormattedText(
                "OBJ: " + (this.model.OtherObjects.Count + 1).ToString(),   // +1 becaulse of the player
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                10,
                Brushes.Gray);
            Point textLocation = new Point(5, this.Height - 25);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw game info.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawInfo(DrawingContext ctx)
        {
            FormattedText text = new FormattedText(
                "Name: " + this.model.PlayerName + " | Difficulty: " + this.model.Difficulty,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.arial,
                10,
                Brushes.Gray);
            Point textLocation = new Point(5, this.Height - 15);
            ctx.DrawText(text, textLocation);
        }

        /// <summary>
        /// Draw The Player.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawPlayer(DrawingContext ctx)
        {
            ctx.DrawGeometry(this.model.Player.GetBrush(), this.transparent, this.model.Player.RealArea);
        }

        /// <summary>
        /// Draw Other Object.
        /// </summary>
        /// <param name="ctx">Drawing Context.</param>
        private void DrawOtherObject(DrawingContext ctx)
        {
            lock (this.model.LockOtherObjects)
            {
                foreach (GameObject obj in this.model.OtherObjects)
                {
                    if (obj.DefaultHP > 0 && obj.CurrentHP > 0 && obj.DefaultHP != obj.CurrentHP)
                    {
                        // Border
                        ctx.DrawGeometry(Brushes.Transparent, this.gray, new RectangleGeometry(new Rect(obj.XPosition - (obj.Width / 2) - 0.5, obj.YPosition - (obj.Height / 2) - 10 - 0.5, (obj.Width / 2) + 0.5, 5 + 0.5)));

                        // Actual hp bar
                        ctx.DrawGeometry(Brushes.OrangeRed, this.transparent, new RectangleGeometry(new Rect(obj.XPosition - (obj.Width / 2), obj.YPosition - (obj.Height / 2) - 10, (obj.Width / 2) * ((double)obj.CurrentHP / (double)obj.DefaultHP), 5)));
                    }

                    ctx.DrawGeometry(obj.GetBrush(), this.transparent, obj.RealArea);
                }
            }
        }
    }
}
