﻿// <copyright file="GameLogics.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GameLogic.GameObjects;
    using GameLogic.Interfaces;

    /// <summary>
    /// Game Logic.
    /// </summary>
    public class GameLogics
    {
        private GameModel model;
        private GameDisplay display;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogics"/> class.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        public GameLogics(GameModel model, GameDisplay display)
        {
            this.model = model;
            this.display = display;
        }

        /// <summary>
        /// Gets or sets the last minute when the player get point.
        /// </summary>
        public int LastMinutePoint { get; set; }

        /// <summary>
        /// Player movement.
        /// </summary>
        /// <param name="speed">Speed in px.</param>
        /// <param name="vertical">Is vertical movement.</param>
        public void PlayerSpeed(double speed, bool vertical = false)
        {
            if (vertical == true)
            {
                this.model.Player.YSpeed += speed;
            }
            else
            {
                this.model.Player.XSpeed += speed;
            }
        }

        /// <summary>
        /// Player movement logic.
        /// </summary>
        public void PlayerMovement()
        {
            // X position
            if (this.model.Player.XPosition - (this.model.Player.Width / 2) + this.model.Player.XSpeed > 0 && this.model.Player.XPosition + this.model.Player.XSpeed < this.display.Width)
            {
                this.model.Player.XPosition += this.model.Player.XSpeed;
            }
            else
            {
                this.model.Player.XSpeed = 0;
            }

            if (this.model.Player.XSpeed != 0)
            {
                this.model.Player.XSpeed += this.model.Player.XSpeed > 0 ? -1 : 1;
            }

            if (this.model.Player.XSpeed > this.model.Player.MaxSpeed)
            {
                this.model.Player.XSpeed = this.model.Player.MaxSpeed;
            }
            else if (this.model.Player.XSpeed < -this.model.Player.MaxSpeed)
            {
                this.model.Player.XSpeed = -this.model.Player.MaxSpeed;
            }

            // Y position
            if (this.model.Player.YPosition - (this.model.Player.Height / 2) + this.model.Player.YSpeed > 0 && this.model.Player.YPosition + this.model.Player.YSpeed < this.display.Height)
            {
                this.model.Player.YPosition += this.model.Player.YSpeed;
            }
            else
            {
                this.model.Player.YSpeed = 0;
            }

            if (this.model.Player.YSpeed != 0)
            {
                this.model.Player.YSpeed += this.model.Player.YSpeed > 0 ? -1 : 1;
            }

            if (this.model.Player.YSpeed > this.model.Player.MaxSpeed)
            {
                this.model.Player.YSpeed = this.model.Player.MaxSpeed;
            }
            else if (this.model.Player.YSpeed < -this.model.Player.MaxSpeed)
            {
                this.model.Player.YSpeed = -this.model.Player.MaxSpeed;
            }
        }

        /// <summary>
        /// OneTick function.
        /// </summary>
        public void OneTick()
        {
            this.PlayerMovement();
            this.model.Player.Collision(this.model);
            List<GameObject> listcopy = new List<GameObject>(this.model.OtherObjects);
            foreach (GameObject item in listcopy)
            {
                if (item is ITicking)
                {
                    (item as ITicking).Ticking(this.model, this.display);
                }

                if (item is INpcControlled)
                {
                    (item as INpcControlled).Decide(this.model);
                }

                if (item is ISolid && !(item is Bullet))
                {
                    (item as ISolid).Collision(this.model);
                }

                if (!item.NotRemoveIfOutOfScreen && this.IsOutOfWindow(item))
                {
                    this.model.OtherObjects.Remove(item);
                }
            }

            // https://stackoverflow.com/questions/26733/getting-all-types-that-implement-an-interface
            foreach (Type spawnable in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                 .Where(mytype => mytype.GetInterfaces().Contains(typeof(ISpawn))))
            {
                var method = spawnable.GetMethod("Spawn");
                if (method != null)
                {
                    method.Invoke(null, new object[] { this.model, this.display });
                }
            }

            Dictionary<PowerupBase, TimeSpan> powerupcopy = new Dictionary<PowerupBase, TimeSpan>(this.model.PlayerPowerups);
            foreach (var powerup in powerupcopy)
            {
                if (TimeSpan.Compare(powerup.Value + powerup.Key.Duration, this.model.TimeElapsed) == -1)
                {
                    powerup.Key.EffectEnd(this.model);
                    this.model.PlayerPowerups.Remove(powerup.Key);
                }
            }

            if ((int)this.model.TimeElapsed.TotalMinutes != this.LastMinutePoint)
            {
                this.model.Points += 100;
                this.LastMinutePoint = (int)this.model.TimeElapsed.TotalMinutes;
            }
        }

        /// <summary>
        /// Checks if the given object is in out of screen.
        /// </summary>
        /// <param name="obj">GameObject.</param>
        /// <returns>Is out of Window?.</returns>
        private bool IsOutOfWindow(GameObject obj)
        {
            return obj.XPosition - obj.Width > this.display.Width ||
                obj.YPosition - obj.Height > this.display.Height ||
                obj.XPosition + obj.Width < 0 ||
                obj.YPosition + obj.Height < 0;
        }
    }
}
