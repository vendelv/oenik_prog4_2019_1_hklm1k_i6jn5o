﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Game Objects.
    /// </summary>
    [Serializable]
    public abstract class GameObject
    {
        /// <summary>
        /// Geometry area object.
        /// </summary>
        [NonSerialized]
        private Geometry area;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// </summary>
        /// <param name="width">Object width.</param>
        /// <param name="height">Object height.</param>
        /// <param name="xPosition">Object X position.</param>
        /// <param name="yPosition">Object Y position.</param>
        /// <param name="hp">Object HP.</param>
        public GameObject(double width, double height, double xPosition, double yPosition, int hp)
        {
            this.Width = width;
            this.Height = height;
            this.XPosition = xPosition;
            this.YPosition = yPosition;
            this.CurrentHP = hp;
            this.DefaultHP = hp;
        }

        /// <summary>
        /// Gets or sets XPosition.
        /// </summary>
        public double XPosition { get; set; }

        /// <summary>
        /// Gets or sets YPosition.
        /// </summary>
        public double YPosition { get; set; }

        /// <summary>
        /// Gets or sets the object rotation in degree.
        /// </summary>
        public double Degree { get; set; }

        /// <summary>
        /// Gets or sets XWidth.
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Gets or sets YWidth.
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Gets or sets the current HP.
        /// </summary>
        public int CurrentHP { get; set; }

        /// <summary>
        /// Gets or sets the max hp.
        /// </summary>
        public int DefaultHP { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets NotRemoveIfOutOfScreen.
        /// </summary>
        public bool NotRemoveIfOutOfScreen { get; set; }

        /// <summary>
        /// Gets or sets the object rotation in Radian.
        /// </summary>
        public double Rad
        {
            get { return this.Degree * Math.PI / 180; }
            set { this.Degree = value / Math.PI * 180; }
        }

        /// <summary>
        /// Gets or sets the object brush image.
        /// </summary>
        public string BrushName { get; set; }

        /// <summary>
        /// Gets the geometry with it's real position.
        /// </summary>
        public Geometry RealArea
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.XPosition, this.YPosition));
                tg.Children.Add(new RotateTransform(this.Degree, this.XPosition, this.YPosition));

                if (this.area == null)
                {
                    this.area = new RectangleGeometry();
                }

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets the geometry in the origo position.
        /// </summary>
        protected Geometry Area { get => this.area; set => this.area = value; }

        /// <summary>
        /// Set the area of the object.
        /// </summary>
        public virtual void SetObjectArea()
        {
            this.Area = new RectangleGeometry(new Rect(-this.Width / 2, -this.Height / 2, this.Width / 2, this.Height / 2));
        }

        /// <summary>
        /// Returns if this and the other GameObject have collision to each other.
        /// </summary>
        /// <param name="other">Other GameObject.</param>
        /// <returns>boolean.</returns>
        public bool IsCollision(GameObject other)
        {
            return Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <summary>
        /// Get the image brush.
        /// </summary>
        /// <returns>Image Brush.</returns>
        public virtual Brush GetBrush()
        {
            string brush = "error.png";
            if (File.Exists("Resources/" + this.BrushName))
            {
                brush = this.BrushName;
            }

            ImageBrush ib = new ImageBrush(BitmapFrame.Create(new Uri("Resources/" + brush, UriKind.Relative)));
            TransformGroup tg = new TransformGroup();
            tg.Children.Add(new RotateTransform(this.Degree, ib.Viewport.Width / 2, ib.Viewport.Height / 2));
            ib.RelativeTransform = tg;
            return ib;
        }
    }
}
