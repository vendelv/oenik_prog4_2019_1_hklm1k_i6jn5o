﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using GameLogic.GameObjects;

    /// <summary>
    /// Game Model.
    /// </summary>
    [Serializable]
    public class GameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="playername">Player's name.</param>
        /// <param name="difficulty">Setted difficulty.</param>
        /// <param name="windowWidth">Window's width.</param>
        /// <param name="windowHeight">Window's height.</param>
        public GameModel(string playername, int difficulty, double windowWidth, double windowHeight)
        {
            this.OtherObjects = new List<GameObject>();
            this.LockOtherObjects = new object();
            this.PlayerPowerups = new Dictionary<PowerupBase, TimeSpan>();
            this.LockPlayerPowerups = new object();
            this.PlayerName = playername;
            this.Difficulty = difficulty;
            this.Player = new Player(100, 100, windowWidth / 2, windowHeight - 10, (int)Math.Ceiling((double)(3.0 / (double)difficulty)));
            this.GameStart = DateTime.Now;
        }

        /// <summary>
        /// Gets the player name.
        /// </summary>
        public string PlayerName { get; private set; }

        /// <summary>
        /// Gets the setted difficulty.
        /// </summary>
        public int Difficulty { get; private set; }

        /// <summary>
        /// Gets or sets Player shoots.
        /// </summary>
        public int Shoots { get; set; }

        /// <summary>
        /// Gets or sets Player points.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets Player killed bosses.
        /// </summary>
        public int KilledBoss { get; set; }

        /// <summary>
        /// Gets or sets Player killed units.
        /// </summary>
        public int KilledUnits { get; set; }

        /// <summary>
        /// Gets or sets the player itself!.
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// Gets or sets other objects.
        /// </summary>
        public List<GameObject> OtherObjects { get; set; }

        /// <summary>
        /// Gets or sets the lock object for OtherObjects.
        /// </summary>
        public object LockOtherObjects { get; set; }

        /// <summary>
        /// Gets or sets player powerups for display.
        /// </summary>
        public Dictionary<PowerupBase, TimeSpan> PlayerPowerups { get; set; }

        /// <summary>
        /// Gets or sets the lock object for PlayerPowerups.
        /// </summary>
        public object LockPlayerPowerups { get; set; }

        /// <summary>
        /// Gets or sets the elapsed gametime.
        /// </summary>
        public TimeSpan TimeElapsed
        {
            get
            {
                return DateTime.Now - this.GameStart;
            }

            set
            {
                this.GameStart = DateTime.Now - value;
            }
        }

        /// <summary>
        /// Gets or sets the save date.
        /// </summary>
        public TimeSpan SavedTimeSpan { get; set; }

        /// <summary>
        /// Gets the game start date.
        /// </summary>
        public DateTime GameStart { get; private set; }
    }
}
