﻿// <copyright file="ISpawn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.Interfaces
{
    /// <summary>
    /// ISpawning. YOU NEED TO MAKE THE FOLLOWING STATIC METHOD IF YOU IMPLEMENT THIS INTERFACE!!!
    /// public static void Spawn(GameModel model, GameDisplay display);
    /// It will be called via reflection at GameLogics.cs.
    /// </summary>
    public interface ISpawn
    {
        // public static void Spawn(GameModel model, GameDisplay display);
    }
}
