﻿// <copyright file="ISolid.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.Interfaces
{
    /// <summary>
    /// This interface indicating whether game object is Solid or not.
    /// </summary>
    public interface ISolid
    {
        /// <summary>
        /// This method must called when the object has collision to an other object.
        /// </summary>
        /// <param name="model">GameModel.</param>
        void Collision(GameModel model);
    }
}
