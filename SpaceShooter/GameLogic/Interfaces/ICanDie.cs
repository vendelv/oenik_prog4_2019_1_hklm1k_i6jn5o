﻿// <copyright file="ICanDie.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Can die.
    /// </summary>
    public interface ICanDie
    {
        /// <summary>
        /// This method calls when the object dies.
        /// </summary>
        void Die();
    }
}
