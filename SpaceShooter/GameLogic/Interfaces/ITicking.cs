﻿// <copyright file="ITicking.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.Interfaces
{
    /// <summary>
    /// IMoveing.
    /// </summary>
    public interface ITicking
    {
        /// <summary>
        /// Moveing logic.
        /// </summary>
        /// <param name="model">GameModel.</param>
        /// <param name="display">GameDisplay.</param>
        void Ticking(GameModel model, GameDisplay display);
    }
}
