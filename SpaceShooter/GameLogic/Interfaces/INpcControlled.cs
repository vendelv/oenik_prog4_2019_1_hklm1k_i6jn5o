﻿// <copyright file="INpcControlled.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using GameLogic.GameObjects;

    /// <summary>
    /// NPC Controlled Objects.
    /// </summary>
    public interface INpcControlled
    {
        /// <summary>
        /// Generates the next action for the NPC.
        /// </summary>
        /// <param name="model">GameModel.</param>
        void Decide(GameModel model);
    }
}
