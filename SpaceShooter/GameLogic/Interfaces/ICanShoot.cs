﻿// <copyright file="ICanShoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    /// <summary>
    /// ICanShoot.
    /// </summary>
    public interface ICanShoot
    {
        /// <summary>
        /// Shoot...
        /// </summary>
        /// <param name="model">GameModel.</param>
        void Shoot(GameModel model);
    }
}
