﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using GameLogic.GameObjects;
    using GameRepository;
    using GameSaveRepository;

    /// <summary>
    /// Game Control object.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private const double FRAMETIME = 16.7;  // 16.7: 60 FPS

        private GameModel model;
        private GameLogics logic;
        private GameDisplay display;
        private DispatcherTimer tickTimer;
        private Stopwatch tickTime;
        private long lastFrameTime;
        private Window win;
        private TimeSpan pauseTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.tickTime = new Stopwatch();
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Gets or sets Player name from GameWindow.xaml.cs.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the Difficulty from GameWindow.xaml.cs.
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// Loads a savefile.
        /// </summary>
        /// <param name="fileName">Save file location.</param>
        public void LoadSaveFile(string fileName)
        {
            LoadLogic loadLogic = new LoadLogic();
            this.model = (GameModel)loadLogic.DeserializeSaveGame(fileName);

            foreach (GameObject item in this.model.OtherObjects)
            {
                item.SetObjectArea();
            }

            foreach (var item in this.model.PlayerPowerups)
            {
                item.Key.SetObjectArea();
            }

            this.model.Player.SetObjectArea();

            // Set back the timer
            this.model.TimeElapsed = this.model.SavedTimeSpan;

            // Pause the game to give time to player
            if (this.tickTimer == null)
            {
                this.tickTimer = new DispatcherTimer();
                this.tickTimer.Interval = TimeSpan.FromMilliseconds(FRAMETIME);
                this.tickTimer.Tick += this.OneTickTimerEvent;
            }

            if (this.win == null)
            {
                this.win = Window.GetWindow(this);
            }

            this.PauseGame();
        }

        /// <summary>
        /// Save the current game.
        /// </summary>
        public void SaveGame()
        {
            SaveLogic saveLogic = new SaveLogic();
            this.model.SavedTimeSpan = this.pauseTime;
            string filename = saveLogic.SerializeSaveGame(this.model);
            (this.win.FindName("SaveFileLabel") as Label).Visibility = Visibility.Visible;
            (this.win.FindName("SaveFileLabel") as Label).Content = $"Game saved to: {filename}";
        }

        /// <summary>
        /// Continue the current game.
        /// </summary>
        public void ContinueGame()
        {
            this.tickTimer.Start();
            this.model.TimeElapsed = this.pauseTime;
            (this.win.FindName("PauseGrid") as Grid).Visibility = Visibility.Hidden;
            (this.win.FindName("SaveFileLabel") as Label).Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Pause the current game.
        /// </summary>
        public void PauseGame()
        {
            this.tickTimer.Stop();
            this.pauseTime = this.model.TimeElapsed;
            (this.win.FindName("PauseGrid") as Grid).Visibility = Visibility.Visible;
        }

        /// <summary>
        /// OnRender override.
        /// </summary>
        /// <param name="drawingContext">Drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.display != null)
            {
                this.display.BuildDisplay(drawingContext, this.lastFrameTime);
            }
        }

        /// <summary>
        /// Event which called when the GameControl is loaded.
        /// </summary>
        /// <param name="sender">Sender object (window).</param>
        /// <param name="e">Event args.</param>
        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.model == null)
            {
                this.model = new GameModel(this.PlayerName, this.Difficulty, this.ActualWidth, this.ActualHeight);
            }

            this.display = new GameDisplay(this.model, this.ActualWidth, this.ActualHeight);
            this.logic = new GameLogics(this.model, this.display);

            if (this.model.SavedTimeSpan != null)
            {
                this.logic.LastMinutePoint = this.model.SavedTimeSpan.Minutes;
            }

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.win = win;

                if (this.tickTimer == null)
                {
                    this.tickTimer = new DispatcherTimer();
                    this.tickTimer.Interval = TimeSpan.FromMilliseconds(FRAMETIME);
                    this.tickTimer.Tick += this.OneTickTimerEvent;
                    this.tickTimer.Start();
                }

                win.Closing += this.Win_Closing;
                win.KeyDown += this.Win_KeyDown;
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Keydown event handler.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Key event args.</param>
        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (this.tickTimer.IsEnabled)
                {
                    this.PauseGame();
                }
                else
                {
                    this.ContinueGame();
                }
            }
        }

        /// <summary>
        /// Event when the game window is closeing.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void Win_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.tickTimer.Stop();
        }

        /// <summary>
        /// Keydown hookup.
        /// </summary>
        private void KeyDownDetect()
        {
            if (Keyboard.IsKeyDown(Key.Left))
            {
                this.logic.PlayerSpeed(-5, false);
            }

            if (Keyboard.IsKeyDown(Key.Right))
            {
                this.logic.PlayerSpeed(5, false);
            }

            if (Keyboard.IsKeyDown(Key.Down))
            {
                this.logic.PlayerSpeed(5, true);
            }

            if (Keyboard.IsKeyDown(Key.Up))
            {
                this.logic.PlayerSpeed(-5, true);
            }

            if (Keyboard.IsKeyDown(Key.Space))
            {
                this.model.Player.Shoot(this.model);
            }

            if (Keyboard.IsKeyDown(Key.T) && Keyboard.IsKeyDown(Key.E) && Keyboard.IsKeyDown(Key.S) && Keyboard.IsKeyDown(Key.L) && Keyboard.IsKeyDown(Key.A))
            {
                if (!this.model.OtherObjects.Exists(x => x is Tesla))
                {
                    this.model.OtherObjects.Add(new Tesla(this.display.Width + 50, new Random().Next((int)this.Height / 2, (int)this.Height)));
                }
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// It calls every tick.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void OneTickTimerEvent(object sender, EventArgs e)
        {
            this.tickTime.Reset();
            this.tickTime.Start();
            this.KeyDownDetect();
            this.logic.OneTick();

            if (this.model.Player.CurrentHP <= 0)
            {
                this.tickTimer.Stop();
                (this.win.FindName("EndGameGrid") as Grid).Visibility = Visibility.Visible;
                this.SavePlayerHighscore(this.model);
            }

            this.InvalidateVisual();
            this.tickTime.Stop();
            this.lastFrameTime = this.tickTime.ElapsedMilliseconds;
        }

        private void SavePlayerHighscore(GameModel model)
        {
            HighScoreCRUDLogic repoLogic = new HighScoreCRUDLogic();

            TimeSpan gameTime = this.model.TimeElapsed;

            HighScoreEntry entry = new HighScoreEntry(
                this.model.PlayerName,
                DateTime.Now,
                this.model.KilledUnits,
                this.model.KilledBoss,
                this.model.Points,
                gameTime.Seconds + (gameTime.Minutes * 60),
                this.model.Difficulty);
            repoLogic.SaveEntry(entry);
        }
    }
}
